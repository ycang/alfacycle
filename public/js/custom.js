$(document).ready(function() {  
	$('.panel-title a').click(function(){
		console.log('hit');
		if($(this).find('i').hasClass('fa-angle-down')){
			$(this).find('i').addClass('fa-angle-right').removeClass('fa-angle-down');
		}else{
			$('.panel-title a i').removeClass('fa-angle-down').removeClass('fa-angle-right');
			$('.panel-title a i').addClass('fa-angle-right'); 
			$(this).find('i').removeClass('fa-angle-right').addClass('fa-angle-down');
		}  
	}); 
	$('.js-add-to-inquiry').click(function(){ 

		var _product_id = $(this).data('product-id'); 
		var envelope = $('.mini-menu .fa-envelope-o').parent();
		var cart_items_count = parseInt($('.cart-items-count').text()) + 1; 
		$('.cart-items-wrapper').removeClass('hide');
		var imgtofly = $(this).parent().parent().parent().siblings('a').find('.item-image img').eq(0);
		envelope.removeClass('animated tada');
	   
    	if (imgtofly) { 
    		if(Cookies.get('inquiryproducts')){
				var product_ids = Cookies.get('inquiryproducts') + ','+_product_id; 
    		}else{
			 	var product_ids = _product_id; 
    		} 
    		Cookies.set('inquiryproducts', product_ids); 
			var imgclone = imgtofly.clone()
				.offset({ top:imgtofly.offset().top, left:imgtofly.offset().left })
				.css({'opacity':'0.7', 'position':'absolute', 'height':'150px', 'width':'150px', 'z-index':'1000'})
				.appendTo($('body'))
				.animate({
					'top':envelope.offset().top + 10,
					'left':envelope.offset().left + 30,
					'width':55,
					'height':55, 
				}, 1000);
			$('.cart-items-count').text(cart_items_count);
			$('.numberhide').html(cart_items_count);
			imgclone.animate({'width':0, 'height':0}, function(){ 
			envelope.addClass('animated tada');$(this).detach(); }); 
		}
		return false;
	}); 
 
    function adjustMenuBar(){ 
        if($('.navbar').length ) {
            if ($(this).scrollTop() > 1){   
                $('.navbar').addClass("sticky"); 
            }
            else{
                $('.navbar').removeClass("sticky"); 
            } 
        }
    }
    adjustMenuBar();
    
 	$(window).scroll(function() { 
        if($('.navbar').length ) {
            if ($(this).scrollTop() > 1){   
                $('.navbar').addClass("sticky"); 
            }
            else{
                $('.navbar').removeClass("sticky"); 
            } 
        }
    });

	$('.js-add-to-inquiry__product-view').click(function(){ 

		var _product_id = $(this).data('product-id'); 
		var envelope = $('.mini-menu .fa-envelope-o').parent();
		var cart_items_count = parseInt($('.cart-items-count').text()) + 1; 
		$('.cart-items-wrapper').removeClass('hide');
		var imgtofly = $('.product-image');
		envelope.removeClass('animated tada');
	   
    	if (imgtofly) { 
    		if(Cookies.get('inquiryproducts')){
				var product_ids = Cookies.get('inquiryproducts') + ','+_product_id; 
    		}else{
			 	var product_ids = _product_id; 
    		} 
    		Cookies.set('inquiryproducts', product_ids); 
			var imgclone = imgtofly.clone()
				.offset({ top:imgtofly.offset().top, left:imgtofly.offset().left })
				.css({'opacity':'0.7', 'position':'absolute', 'height':'150px', 'width':'150px', 'z-index':'1000'})
				.appendTo($('body'))
				.animate({
					'top':envelope.offset().top + 10,
					'left':envelope.offset().left + 30,
					'width':55,
					'height':55, 
				}, 1000);
			$('.cart-items-count').text(cart_items_count);
			$('.numberhide').html(cart_items_count);
			imgclone.animate({'width':0, 'height':0}, function(){ 
			envelope.addClass('animated tada');$(this).detach(); }); 
		}
		return false;
	}); 

	$('.js-logout').click(function(){ 
		Cookies.remove('inquiryproducts');
		window.location.replace("/member/logout");
		return false;
	});
	$('.mini-search-form input').focus(function() {
	  $(this).css( "visibility", "visible" );
	});
	 $("img").one("load", function() {
        $(this).show();
    }).each(function() {
        if (this.complete) $(this).load();
    }).error(function () {  
        $(this).unbind("error").attr("src", "/img/No_image_available.jpg");
    });
});