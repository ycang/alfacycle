<?php
$cart_items_count = 0 ;
if(isset($_COOKIE["inquiryproducts"])):
    $inquiryproducts_arr = explode(',', $_COOKIE["inquiryproducts"]); 
    $inquiryproducts_arr = array_unique($inquiryproducts_arr);  
    $cart_items_count = count($inquiryproducts_arr); 
endif; 
use App\Models\Category; 
$parent_categories = Category::where('status','2')->where('parent_id',0)->get(); 

?>
 <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="nav-1 transition"> 
            <div class="container">
                <div class="row">
                    <div class="col-xs-1 pull-right mini-menu" style="position: relative;left: -10px;">
                        <div class=" hide numberhide"></div>
                        <a href="{{ url('inquiry')}}" style="position:relative;top:10px;"> 
                        
                        <span style="position:absolute;" class="fa-stack fa-lg cart-items-wrapper @if ($cart_items_count == 0 )
                         hide
                        @endif">
                          <i class="fa fa-circle fa-stack-2x text-red" style="font-size:25px;margin-top: 5px;"></i>
                          <i class="fa fa-stack-1x fa-inverse cart-items-count">{{$cart_items_count}}</i>
                        </span> 
                        
                        <i class="fa fa-envelope-o text-white "></i></a>
                    </div> 
                    <div class="col-xs-10 col-sm-5 pull-right mini-menu">
                        <div class="row">
                         <form method="put" role="search" action="{{url('searchresult')}}" class="mini-search-form"> 
                            <div class="col-xs-5 " style="padding-left:0px;"> 
                                <select name="category" class="inline form-control mini-search-keyword" style="width: 100%;right: 0px;float: right;">
                                <option value=''>All Category</option>
                                @foreach($parent_categories as $parent_category)  
                                <option value={{$parent_category->id}}>{{$parent_category->translate(Session::get('locale'))->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-xs-5 nopadding">
                                <input type="text" class="inline form-control mini-search-keyword" name="keyword" placeholder="Keyword...">
                            </div>
                            <div class="col-xs-2 text-center">
                                <button type="submit" class="inline" style="background-color: transparent;border: 0px;padding-top:10px;"><i class="fa fa-search text-white"></i></button>
                            </div>
                            </form> 
                        </div>  
                    </div>
                </div> 
            </div>
        </div>
        <div class="nav-2"> 
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                                
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header page-scroll">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand transition" href="{{ url('/')}}"><img src="{{asset('img/logo.png')}}" height="70px" /></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse transition" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right w3_megamenu">
                                <li class="hidden">
                                    <a href="#page-top"></a>
                                </li>
                                <li class="page-scroll">
                                    <a href="{{ url('/')}}">HOME</a>
                                </li>  
                                @foreach($parent_categories as $parent_category)  
                                    <?php 
                                        $sub_categories = Category::where('status','2')->where('parent_id',$parent_category->id)->get();  
                                    ?>
                                    <li class="dropdown w3_megamenu-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">{{$parent_category->translate(Session::get('locale'))->name}}<b class="caret"></b></a>
                                        <ul class="dropdown-menu fullwidth">
                                            <li class="w3_megamenu-content withdesc">
                                                <div class="row">
                                                    @foreach($sub_categories as $sub_category)  
                                                    <div class="col-sm-3">
                                                        <h3 class="title"><a href="{{url('catalog/category/'.$sub_category->id)}}">{{$sub_category->translate(Session::get('locale'))->name}}</a></h3>
                                                    </div>
                                                    @endforeach 
                                                </div>
                                            </li>
                                        </ul>
                                    </li> 
                                @endforeach
                                <li class="page-scroll">
                                    <a href="{{ url('contact-us')}}">CONTACT US</a>
                                </li>
                                @if(Auth::user()) 
                                <li class="page-scroll dropdown">
                                    <a class="dropdown-toggle text-red" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="text-transform:uppercase;">
                                      {{Auth::user()->name}} <!--<span class="caret"></span>-->
                                    </a> 
                                    <!--
                                    <ul class="dropdown-menu"> 
                                        <li><a href="{{ url('member/logout')}}">Logout</a></li>
                                    </ul>-->
                                </li>
                                 <li class="page-scroll"><a href="{{ url('member/logout')}}" class="js-logout text-red">LOGOUT</a></li>
                                @else
                                <li class="page-scroll">
                                    <a href="{{ url('member/login')}}" class="text-red">LOGIN</a>
                                </li>
                                <li class="page-scroll">
                                    <a href="{{ url('member/register')}}" class="text-red">SIGNUP</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                </div> 
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header> 
    </header>
