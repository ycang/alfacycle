<!DOCTYPE html>
<html>
	@include('partials.frontend.htmlheader')

	<body> 
		<!-- Navigation -->
		@include('partials.frontend.menu')
		<!-- Main content -->
		<section class="content"> 
		@yield('frontend-content')
		</section>

	    <footer class="text-center"> 
	        <div class="footer-below">
	            <div class="container">
	                <div class="row">
	                    <div class="col-lg-12 text-right text-white">
	                       &copy; Alfa Cycle Supply Company. All Rights Reserved
	                    </div>
	                </div>
	            </div>
	        </div>
	    </footer>

		<!-- Scripts -->
		@include('partials.frontend.scripts')
	</body>

</html>