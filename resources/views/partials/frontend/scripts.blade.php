<script src="{{ asset('js/jquery-2.1.4.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>  
<script src="{{ asset('js/js.cookie.js') }}"></script>   
<script src="{{ asset('js/custom.js') }}?v=2"></script> <!-- Resource jQuery -->
<!-- Addon Scripts -->
@yield('frontend-addon-script')