<div class="row"> 
      <strong class="col-xs-12 col-md-6 alert-wrapper">
        @if(Session::has('success')) 
            <div class="alert alert-success" style="margin-top:20px;">
            <a class="close" data-dismiss="alert">×</a> 
                <p>{!! Session::get('success') !!}</p>  
            </div> 
        @endif
         @if(Session::has('error')) 
            <div class="alert alert-danger" style="margin-top:20px;">
            <a class="close" data-dismiss="alert">×</a> 
                <p>{!! Session::get('error') !!}</p>  
            </div> 
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger" style="margin-top:20px;">
            <a class="close" data-dismiss="alert">×</a> 
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </strong>
</div>