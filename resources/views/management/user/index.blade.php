@extends('app')

@section('htmlheader_title')
Members Management
@endsection

@section('contentheader_title')
Members Management
@endsection

@section('contentheader_description')
Description for members management
@endsection

@section('main-content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Member</h3> 
				<a href="{{url('/admin/manage/user?show=all')}}" class="btn btn-primary pull-right">Show All Members</a>
			</div>
			<div class="box-body">
				<table id="tbl-category" class="table">
					<thead>
						<th>ID</th>  
						<th>Name</th>  
						<th>Email</th> 
						<th>Company</th>   
						<th>Gender</th>   
						<th>Address,Country</th>  
						<th>Status</th> 
						<th>Created At</th>
						<th>Action</th>
					</thead>
					<tbody>
						@if (isset($users) && !$users->isEmpty())
						@foreach ($users as $user)
						<tr>
							<td>{{ $user->id }}</td>   
							<td class="col-xs-5">{{ $user->name }}</td> 
							<td class="col-xs-5">{{ $user->email }}</td>  
							<td class="col-xs-5">Company : {{ $user->company }}<br/>Fax : {{ $user->fax }}<br/> Telephone : {{ $user->telephone }}</td>
							<td class="col-xs-5">
							{{ $user->gender }}</td> 
							<td class="col-xs-5">{{ $user->address }}<br/>{{ $user->country }}</td>
							<td>
								@if ($user->status == '2')
								<span class="label label-success">Active</span>
								@elseif ($user->status == '1')
								<span class="label label-danger">Inactive</span>
								@else 
								<span class="label label-warning">Incomplete</span>
								@endif
							</td>
							<td class="col-xs-5">{{ $user->created_at }}</td>
							<td>
								@if ($user->status == '2')
								<a href="{{ url('admin/manage/user/setInactive/' . $user->id) }}" class="btn btn-default">Set Inactive</a>
								@elseif ($user->status == '1')
								<a href="{{ url('admin/manage/user/setActive/' . $user->id) }}" class="btn btn-default">Set Active</a>
								@endif
							</td>
						</tr>
						@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('addon-script')
<script type="text/javascript">
$(document).ready(function()
{
	$('#tbl-category').DataTable();
});
</script>
@endsection