@extends('app')

@section('htmlheader_title')
Project Management
@endsection

@section('contentheader_title')
Project Management
@endsection

@section('contentheader_description')
Description for catalog management
@endsection

@section('main-content') 
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form method="POST" action="{{ url('manage/catalog/massdelete/') }}" accept-charset="UTF-8" >
			<input name="_token" type="hidden" value="{{{ csrf_token() }}}" />
			<div class="box-header with-border">
				<h3 class="box-title">Project</h3>
				<ul class="list-inline  pull-right">
					<li><a href="{{ url('admin/manage/catalog/create') }}" class="btn btn-primary">Create</a></li>
					<li><button type="submit" class="btn btn-danger">Delete Selected</button></li>
				</ul>  
			</div>
			<div class="box-body"> 
				<table id="tbl-catalog" class="table">
					<thead>
						<th></th>
						<th>ID</th> 
						<th>Catalog Name</th> 
						<th>Category</th> 
						<th>Status</th>
						<th>Action</th>
					</thead>
					<tbody>
						@if (isset($catalogs) && !$catalogs->isEmpty())
						@foreach ($catalogs as $catalog)
						<tr>
							<td><input type="checkbox" name="id[]" value="{{ $catalog->id }}"</td>
							<td>{{ $catalog->id }}</td> 
							<td>{{ $catalog->name }}</td> 
							<td>	
							@foreach ($categories as $category)
								@if(isset($catalog->category_id) && $catalog->category_id == $category->id)
									{{ $category->name }}
								@endif
							@endforeach
							</td> 
							<td>
								@if ($catalog->status == '2')
								<span class="label label-success">Active</span>
								@elseif ($catalog->status == '1')
								<span class="label label-danger">Inactive</span>
								@else 
								<span class="label label-warning">Incomplete</span>
								@endif
							</td>
							<td>
								@if ($catalog->status == '2')
								<a href="{{ url('admin/manage/catalog/setInactive/' . $catalog->id) }}" class="btn btn-default">Set Inactive</a>
								@elseif ($catalog->status == '1')
								<a href="{{ url('admin/manage/catalog/setActive/' . $catalog->id) }}" class="btn btn-default">Set Active</a>
								@endif
								<a href="{{ url('admin/manage/catalog/edit/' . $catalog->id) }}" class="btn btn-default">Edit</a>
								<a onclick="javascript: if (confirm('Are you sure you want to delete this?')) { href='{{ url('admin/manage/catalog/destroy/' . $catalog->id) }}'} else { alert('Delete Cancelled.');return false; }; "  href="#"  class="btn btn-danger">Delete</a>
							</td>
						</tr>
						@endforeach
						@endif
					</tbody>
				</table> 
			</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('addon-script')
<script type="text/javascript">
$(document).ready(function()
{
	$('#tbl-catalog').DataTable();
});
</script>
@endsection