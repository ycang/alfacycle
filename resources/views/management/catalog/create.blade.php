@extends('app')
<?php
use App\Models\Status;
use App\Models\Locale;
use App\Models\Files;
use App\Models\Category;

$locales = Locale::where('status', '=', STATUS::ACTIVE)->get(); 
?>

@section('htmlheader_title')
Catalog Management
@endsection

@section('contentheader_title')
Catalog Management
@endsection

@section('contentheader_description')
Description for catalog management
@endsection

@section('main-content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Create New Catalog</h3>
			</div>
			<form method="POST" action="{{ url('manage/catalog/store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
				<input name="_token" type="hidden" value="{{{ csrf_token() }}}" />
				<div class="box-body">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
						@if (isset($locales) && !$locales->isEmpty())
							<?php $count = 0; ?>
							@foreach ($locales as $locale)
								<?php $count++; ?>
								<li class="{{ ($count == 1) ? 'active' : '' }}"><a href="#{{ $locale->language }}" data-toggle="tab">{{ strtoupper($locale->language) }}</a></li>
							@endforeach
						@endif
						</ul>
						<div class="tab-content">
						@if (isset($locales) && !$locales->isEmpty())
							<?php $count = 0; ?>
							@foreach ($locales as $locale)
								<?php $count++; ?>
								<div id="{{ $locale->language }}" class="tab-pane {{ ($count == 1) ? 'active' : '' }}">
									<div class="form-group">
										<label for="name" class="control-label">Name</label>
										<input type="text" id="name" name="name[{{ $locale->id }}]" type="text" class="form-control" rows="4" required/>
									</div>
									<div class="form-group">
										<label for="description" class="control-label">Description</label>
										<textarea id="description" name="description[{{ $locale->id }}]" type="text" class="form-control" rows="4" required></textarea>
									</div>
								</div>
							@endforeach
						@endif
						</div>
					</div>
					<div class="form-group">
						<label for="category_id" class="control-label">Category</label>
						<select id="category_id" name="category_id" class="form-control"> 
						<?php echo $categories;?>
						</select>
					</div> 
					<div class="form-group">
						<label for="slug" class="control-label">URL Key</label>
						<input id="slug" name="slug" type="text" class="form-control" value="" required />
					</div>
					<div class="form-group">
						<label for="year" class="control-label">Year</label>
						<input id="year" name="year" type="text" class="form-control" value="" required />
					</div>
					<div class="form-group">
						<label for="sort_order" class="control-label">Sort Order</label>
						<input id="sort_order" name ="sort_order" type="text" class="form-control" />
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="thumbnail">
								<img id="grid_img" class="img-thumbnail" src="" alt="" width="100%">
								<div class="caption" style="text-align: center;">
									<p><strong>Grid Front Image</strong></p>
									<div class="row">
										<div class="col-xs-4">
											<a href="#" class="btn btn-block btn-primary" role="button" data-toggle="modal" data-target="#modal-grid-img"><i class="fa fa-cloud-upload"></i> Upload</a> 
										</div>
										<div class="col-xs-4">
											<a href="#" class="btn btn-block btn-default" role="button" data-toggle="modal" data-target="#modal-upload" onclick="useExist('grid_img_id')"><i class="fa fa-image"></i> Gallery</a>
										</div> 
										<div class="col-xs-4">
											<a href="#" class="btn btn-block btn-danger" role="button" onclick="removeImg('grid_img_id', 'grid_img');return false;"><i class="fa fa-remove"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" id="grid_img_id" name="grid_img_id" /> 
					</div>
				</div> 
				<!-- Grid Image - Modal -->
				<div class="modal fade" id="modal-grid-img" tabindex="-1" role="dialog" aria-labelledby="modal-grid-img">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="modal">Upload new grid front image</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="new_grid_img_id" class="control-label">New Grid Front Image</label>
									<input type="file" class="form-control" id="new_grid_img_id" name="new_grid_img_id" />
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>
							</div>
						</div>
					</div>
				</div> 
				<div class="box-footer clearfix">
					<div class="pull-right">
						<a href="{{ url('/admin/manage/project/') }}" class="btn btn-default">Cancel</a>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div> 
			</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-upload" tabindex="-1" role="dialog" aria-labelledby="modal-upload">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal">Choose from existing image collection</h4>
			</div>
			<div id="lazy-container" class="modal-body" style="max-height: 450px; overflow-y: auto;">
				<?php $count = 0; ?>
				<?php $images = Files::where('status', '=', STATUS::ACTIVE)->get(); ?>
				@foreach ($images as $image)
				<?php $count++; ?>
				@if ($count % 4 == 1)
				<div class="row">
				@endif
					<div class="col-xs-6 col-md-3">
						<button class="thumbnail" data-dismiss="modal" onclick="selectImg({{ $image->id }}, '{{ $image->dir }}')">
							@if (isset($image->type) && substr($image->type, 0, 5) == 'image')
							<img data-original="{{ $image->dir }}" alt="{{ $image->name }}" class="img-thumbnail lazy-img" width="100%">
							<?php /*
							@elseif (isset($image->type) && substr($image->type, 0, 5) == 'video')
							<video width="100%">
								<source src="{{ $image->dir }}" />
							</video>
							*/ ?>
							@endif
						</button>
					</div>
				@if (($count % 4 == 0) || ($count == $images->count())) 
				</div>
        		@endif
				@endforeach
			</div>
		</div>
	</div>
</div>
 
@endsection

@section('addon-script')
<script type="text/javascript">
$(document).ready(function()
{
	$('.colorpicker-element').colorpicker();

	$('.img_sort_order').on('keyup', function()
	{
		var fields = $('#current-modal-field').val().split(',');
		setImgSortOrder(fields[1]);
	});
});

function useExist(imgType)
{
	$(function()
	{
		$('.lazy-img').lazyload(
		{
			container: '#lazy-container'
		});
	});

	$('#selected_img').val(imgType);
}

function removeImg(val, src)
{
	$('#' + src).attr('src', '');
	$('#' + src).attr('alt', '');
	$('#' + val).val('');
}

function selectImg(imgId, imgSrc)
{
	var imgType = $('#selected_img').val();

	if (imgType != '')
	{
		$('#' + imgType).val(imgId);

		if (imgType == 'grid_img_id')
		{
			$('#grid_img').attr('src', imgSrc);
		}
		else if (imgType == 'grid_bg_img_id')
		{
			$('#grid_bg_img').attr('src', imgSrc);
		}
		else if (imgType == 'brand_img_id')
		{
			$('#brand_img').attr('src', imgSrc);
		}
		else if (imgType == 'mascott_img_id')
		{
			$('#mascott_img').attr('src', imgSrc);
		}
		else if (imgType == 'video_img_id')
		{
			$('#video_img').attr('src', imgSrc);
		}
		else
		{
			$('input.' + imgType).val(imgId);
			$('img.' + imgType).attr('src', imgSrc);
		}
	}
}

function selectProjectImg()
{
	var imgIds  = [];
	var imgUrl  = [];
	var imgType = [];
	var fields = $('#current-modal-field').val().split(',');

	$('.project_img').each(function()
	{
		var imgId = $(this).val();

		if ($(this).is(':checked'))
		{
			imgIds.push(imgId);
			$('#img_sort_order_container_' + imgId).show();
			imgUrl.push($(this).data('img'));
			imgType.push($(this).data('type'));
		}
		else
		{
			$('#img_sort_order_' + imgId).val('');
			$('#img_sort_order_container_' + imgId).hide();
		}
	});

	$('#' + fields[0]).val(imgIds.join(','));
	setImgSortOrder(fields[1]);

	var thumbnails = '';
	for (var x = 0; x < imgUrl.length; x++)
	{
		if (imgType[x] == 'image')
		{
			thumbnails += '<div class="col-md-3"><img src="'+imgUrl[x]+'" class="img-thumbnail" width="100%" /></div>';
		}
		else if (imgType[x] == 'video')
		{
			thumbnails += '<div class="col-md-3"><video width="100%" controls><source src="'+imgUrl[x]+'" /></video></div>';
		}
	}
	$('#' + fields[2]).html(thumbnails);
}

function setImgSortOrder(field)
{
	var sortOrder = [];

	$('.img_sort_order').each(function()
	{
		if ($(this).closest('.thumbnail').find('.project_img').is(':checked'))
		{
			if ($(this).val() != '')
			{
				sortOrder.push($(this).val());
			}
			else
			{
				sortOrder.push(0);
			}
		}
	});

	$('#' + field).val(sortOrder.join(','));
}

function addProjectImg(cnt)
{
	$('#new_project_img_container_'+cnt).append('<input type="file" class="form-control new_project_img" id="new_project_img_id" name="new_project_img_id['+cnt+'][]" />');
}

function prepareModal(img, sort, thumbnail)
{
	$(function()
	{
		$('.lazy-project-img').lazyload(
		{
			container: '#lazy-project-container'
		});
	});
	
	$('#current-modal-field').val(img + "," + sort + "," + thumbnail);

	var imgIds = $('#' + img).val().split(',');
	var sorts  = $('#' + sort).val().split(',');
	var count  = 0;

	$('.project_img').each(function()
	{
		if (imgIds.length > 0 && $.inArray($(this).val(), imgIds) != -1)
		{
			$(this).prop('checked', true);
			$('#img_sort_order_' + $(this).val()).val(sorts[count]);
			$('#img_sort_order_container_' + $(this).val()).show();
			count++;
		}
		else
		{
			$(this).prop('checked', false);
			$('#img_sort_order_' + $(this).val()).val('');
			$('#img_sort_order_container_' + $(this).val()).hide();
		}
	});
}

function addBlock()
{
	var count = parseInt($('#block-count').val());
	$('#block-count').val(count+1);

	var blockInput = '<div id="new_block_'+count+'"><div class="box-header with-border">'
		+ '<h4 class="box-title">Block #' + (count+1) + '</h4>'
		+ '<button type="button" class="btn btn-danger pull-right" onclick="removeBlock('+count+', \'\')">Remove</button>'
		+ '</div>'
		+ '<div class="form-group">'
		+ '<label for="block-type" class="control-label">Type</label>'
		+ '<select id="block-type-'+count+'" class="form-control block-type" name="block[type][]" onchange="">'
		+ '<option value="img">Single Image</option>'
		+ '<option value="vid">Video</option>'
		+ '<option value="gal">Gallery</option>'
		+ '</select>'
		+ '</div>'
		+ '<div id="selected-img-container-'+count+'" class="row"></div>'
		+ '<div class="form-group">'
		+ '<label for="block-value-'+count+'" class="control-label">Value</label>'
		+ '<div class="input-group">'
		+ '<input type="text" id="project_img_ids_'+count+'" class="form-control" name="block[value][]" readonly />'
		+ '<input type="hidden" id="project_img_sort_order_'+count+'" name="project_img_sort_order[]" />' 
		+ '<span class="input-group-btn">'
		+ '<button type="button" id="btn-upload-'+count+'" class="btn btn-primary" data-toggle="modal" data-target="#modal-new-project-img-'+count+'">Upload</button>'
		+ '<button type="button" id="btn-choose-'+count+'" class="btn btn-default" data-toggle="modal" data-target="#modal-project-img" onclick="prepareModal(\'project_img_ids_'+count+'\', \'project_img_sort_order_'+count+'\',\'selected-img-container-'+count+'\')">Choose</button>'
		+ '</span>'
		+ '</div>'
		+ '</div>'
		+ '<div class="form-group">'
		+ '<label for="block-sort" class="control-label">Sort Order</label>'
		+ '<input type="text" id="project_block_sort_'+count+'" class="form-control" name="block[sort][]" />'
		+ '</div>'
		+ '<div class="row">'
		+ '<div class="col-md-4">'
		+ '<img class="img-thumbnail thumbnail_'+count+'" src="" alt="" width="100%">'
		+ '</div>'
		+ '</div>'				
		+ '<div class="form-group">'
		+ '<label for="block-sort" class="control-label">Thumbnail</label>'
		+ '<div class="input-group">'
		+ '<input type="text" id="thumbnail_'+count+'" class="form-control thumbnail_'+count+'" name="block[thumbnail]['+count+']" readonly />'
		+ '<span class="input-group-btn">'
		+ '<button type="button" id="btn-upload-'+count+'" class="btn btn-primary" data-toggle="modal" data-target="#modal-thumbnail-'+count+'">Upload</button>'
		+ '<button type="button" id="btn-choose-'+count+'" class="btn btn-default" data-toggle="modal" data-target="#modal-upload" onclick="useExist(\'thumbnail_'+count+'\')">Choose</button>'
		+ '</span>'
		+ '</div>'
		+ '</div>'
		+ '<hr />'
		+ '<div class="modal fade" id="modal-new-project-img-'+count+'" tabindex="-1" role="dialog" aria-labelledby="modal-new-project-img">'
		+ '<div class="modal-dialog" role="document">'
		+ '<div class="modal-content">'
		+ '<div class="modal-header">'
		+ '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
		+ '<h4 class="modal-title" id="modal">Upload new project images</h4>'
		+ '</div>'
		+ '<div class="modal-body">'
		+ '<div class="form-group" id="new_project_img_container_'+count+'">'
		+ '<label for="new_project_img_id" class="control-label">New Project Image</label>'
		+ '<input type="file" class="form-control new_project_img" id="new_project_img_id" name="new_project_img_id['+count+'][]" />'
		+ '</div>'
		+ '</div>'
		+ '<div class="modal-footer">'
		+ '<button type="button" class="btn btn-default" onclick="addProjectImg('+count+')">Add More</button>'
		+ '<button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '<div class="modal fade" id="modal-thumbnail-'+count+'" tabindex="-1" role="dialog" aria-labelledby="modal-thumbnail">'
		+ '<div class="modal-dialog" role="document">'
		+ '<div class="modal-content">'
		+ '<div class="modal-header">'
		+ '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
		+ '<h4 class="modal-title" id="modal">Upload new thumbnails</h4>'
		+ '</div>'
		+ '<div class="modal-body">'
		+ '<div class="form-group" id="new_thumbnail_container_'+count+'">'
		+ '<label for="new_thumbnail" class="control-label">New Thumbnail</label>'
		+ '<input type="file" class="form-control new_thumbnail" id="new_thumbnail" name="new_thumbnail['+count+'][]" />'
		+ '</div>'
		+ '</div>'
		+ '<div class="modal-footer">'
		+ '<button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>';
	$('#block-box-body').append(blockInput);
}

function toggleInput(count, select)
{
	if ($(select).val() == 'vid')
	{
		$('#project_img_ids_' + count).attr('readonly', false);
		$('#btn-upload-' + count).attr('disabled', true);
		$('#btn-choose-' + count).attr('disabled', true);
	}
	else
	{
		$('#project_img_ids_' + count).attr('readonly', true);
		$('#btn-upload-' + count).attr('disabled', false);
		$('#btn-choose-' + count).attr('disabled', false);
	}
}

function removeBlock(count)
{
	$('#new_block_' + count).hide();
	$('#block-type-' + count).attr('disabled', true);
	$('#project_img_ids_' + count).attr('disabled', true);
	$('#project_img_sort_order_' + count).attr('disabled', true);
	$('#project_block_sort_' + count).attr('disabled', true);
	$('#new_project_img_container_' + count).find('.new_project_img').attr('disabled', true);
	$('#thumbnail_' + count).attr('disabled', true);
	$('#new_thumbnail_container_' + count).find('.new_thumbnail').attr('disabled', true);
}
</script>
@endsection