@extends('app')
<?php
use App\Models\Status;
use App\Models\Locale;
use App\Models\Files;
use App\Models\Category;

$locales = Locale::where('status', '=', STATUS::ACTIVE)->get(); 
?>

@section('htmlheader_title')
Catalog Management
@endsection

@section('contentheader_title')
Catalog Management
@endsection

@section('contentheader_description')
Description for catalog management
@endsection

@section('main-content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Import New Catalog</h3>
			</div>
			<form method="POST" action="{{ url('manage/catalog/import') }}" accept-charset="UTF-8" enctype="multipart/form-data">
				<input name="_token" type="hidden" value="{{{ csrf_token() }}}" />
				<input name="category_id" type="hidden" value="{{$category_id}}" /> 
				<div class="box-body"> 
					<div class="form-group">
						<label for="file" class="control-label">File (csv)</label>
						<input id="file" name="file" type="file" class="form-control" value="" required />
					
					</div>  
				</div> 
				<div class="box-footer clearfix">
					<div class="pull-right">
						<a href="{{ url('/admin/manage/category/') }}" class="btn btn-default">Cancel</a>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div> 
			</form>
		</div>
	</div>
</div>
 
@endsection