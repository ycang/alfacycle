@extends('app')

@section('htmlheader_title')
Category Management
@endsection

@section('contentheader_title')
Category Management
@endsection

@section('contentheader_description')
Description for category management
@endsection

@section('main-content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Category</h3>
				<a href="{{ url('admin/manage/category/create') }}" class="btn btn-primary pull-right">Create</a>
			</div>
			<div class="box-body">
				<ul>
				<?php echo $categories;?>
				</ul>
			</div>
		</div>
	</div>
</div>
@endsection

@section('addon-script')
<script type="text/javascript">
$(document).ready(function()
{
	$('.js-collapse').click(function(){
		$(this).siblings('ul').toggle();
		return false;
	});
});
</script>
@endsection