@extends('partials.frontend.app')

@section('frontend-content')
<style type="text/css">
    .login-container{height:100%;width: 100%;background-size: cover;background-position: 50% 0%;background-image: url({{asset('img/img_sgnInUp.jpg')}})}
section{height: 100%;}
</style>
<div class="container login-container"> 
    @include('partials.frontend.systemMessage')   

    <div class="col-xs-12 col-md-6 pull-right" style="margin-top:6%;">
        <div class="row">
            <h2 class="col-md-12 text-center font-rockwell">Member Login</h2>
        </div>
        <form action="{{ url('/member/login') }}" method="post" class="login-form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
            <div class="row">
            <div class="form-group col-md-8 col-md-offset-2">
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
            </div></div>

            <div class="row">
            <div class="form-group col-md-8 col-md-offset-2">
                <input type="password" class="form-control" placeholder="Password" name="password"/>
            </div> 
            </div>
            <div class="row"> 
                <div class="col-xs-4 col-md-offset-4" style="padding-top:30px;">
                    <button type="submit" class="btn btn-black btn-lg btn-block btn-flat">LOGIN</button>
                </div><!-- /.col -->
            </div>
        </form> 
        <div class="row">
            <div class="col-xs-12 text-center" style="padding-top:30px;">
            <a href="{{url('member/password/email')}}">FORGOT PASSWORD?</a>
            <p>Not a member? <a href="{{url('member/register')}}">SIGN UP HERE</a></p>
            </div> 
        </div> 
    </div><!-- /.form-box -->   

</div><!-- /.login-box-body --> 

@endsection
