@extends('partials.frontend.app')

@section('frontend-content') 

<style type="text/css">
    .register-container{height:100%;width: 100%;background-size: cover;background-position: 50% 0%;background-image: url({{asset('img/img_sgnInUp.jpg')}})}
    section{height: 100%;}
</style>
<div class="container register-container"> 
    @include('partials.frontend.systemMessage')   
    <div class="col-xs-12 col-md-6 pull-right" style="margin-top:1%;">
        <div class="row">
            <h2 class="col-md-12 text-center font-rockwell">Member Sign Up</h2> 
        </div>
        <form action="{{ url('/member/register') }}" method="post" class="register-form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
            <div class="form-group col-md-8 col-md-offset-2">
                <small class="text-red">* Please fill in all fields</small>
                <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}"/>
            </div> 
            </div>
            <div class="row">
            <div class="form-group col-md-8 col-md-offset-2">
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
            </div></div>

            <div class="row">
            <div class="form-group col-md-4 col-md-offset-2">
                <input type="password" class="form-control" placeholder="Password" name="password"/>
            </div>
            <div class="form-group col-md-4">
                <input type="password" class="form-control" placeholder="Retype Password" name="password_confirmation"/>
            </div>
            </div>
            <hr style="border:1px solid #000; width:80% "/>
            <div class="row">
                <div class="form-group col-md-8 col-md-offset-2">
                    <input type="text" class="form-control" placeholder="Company" name="company"  value="{{ old('company') }}"/>
                </div> 
            </div>
            <div class="row">
                <div class="form-group col-md-8 col-md-offset-2">
                    <input type="text" class="form-control" placeholder="Website" name="website"  value="{{ old('website') }}"/>
                </div>
            </div> 
            <div class="row">
                <div class="form-group col-md-4 col-md-offset-2">
                    <input type="text" class="form-control" placeholder="Telephone" name="telephone"  value="{{ old('telephone') }}"/>
                </div> 
                <div class="form-group col-md-4">
                    <input type="text" class="form-control" placeholder="Country" name="country"  value="{{ old('country') }}"/>
                </div> 
            </div>
            <div class="row">
                <div class="form-group col-md-8 col-md-offset-2">
                    <input type="text" class="form-control" placeholder="Address" name="address" value="{{ old('address') }}"/>
                </div>
            </div>
            <div class="row"> 
                <div class="col-xs-4 col-md-offset-4" style="padding-top:20px;">
                    <button type="submit" class="btn btn-black btn-lg btn-block btn-flat">Sign Up</button>
                </div><!-- /.col -->
            </div>
        </form>  
        <div class="row">
            <div class="col-xs-12 text-center" style="padding-top:30px;"> 
            <p>Already a member? <a href="{{url('member/login')}}">LOGIN HERE</a></p>
            </div> 
        </div>
    </div><!-- /.form-box -->  

</div>
@endsection
