@extends('partials.frontend.app')

@section('frontend-content')
<style type="text/css">
    .login-container{height:100%;width: 100%;background-size: cover;background-position: 50% 0%;background-image: url({{asset('img/img_sgnInUp.jpg')}})}
section{height: 100%;}
</style>
<div class="container login-container"> 
    @include('partials.frontend.systemMessage')   

    <div class="col-xs-12 col-md-6 pull-right" style="margin-top:6%;">
        <div class="row">
            <h2 class="col-md-12 text-center font-rockwell">Forgot Password</h2>
        </div> 
            <form action="{{ url('/member/password/email') }}" method="post" class="email-reset-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                <div class="form-group col-md-8 col-md-offset-2">
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
                 </div>
 
                <div class="row"> 
                    <div class="col-xs-4 col-md-offset-4">
                        <button type="submit" class="btn btn-black btn-lg">Send Password Reset</button>
                    </div><!-- /.col -->
                </div>
            </form> 
        </div> 
    </div><!-- /.form-box -->   

</div><!-- /.login-box-body --> 

@endsection
