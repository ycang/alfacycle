@extends('partials.frontend.app')

@section('frontend-content')   
<?php  
use App\Models\Files;
use App\Models\Setting;
$homepage_bg_img = Setting::where('code', '=', 'homepage_bg_img')->first()->value; 
?>
<style type="text/css">
    .home-container{padding-top:5%;height:100%;width: 100%;background-size: cover;background-position: 50% 0%;background-image: url({{ ($homepage_bg_img != '') ? Files::find($homepage_bg_img)->dir : '' }})}
</style>
<div class="container home-container" >
    @include('partials.frontend.systemMessage')    
    <div class="row">
	<div class="col-sm-5 col-sm-push-1"> 
    	<div class="row">
    		<div class="col-md-10 col-xs-12"> 
		    	<div class="home-finder-wrapper">
		    		<div class="home-finder-heading" style="border-bottom:2px solid #f42624;padding:10px 30px 7px 30px;"> 
		    			<div class="text-white"><h2><b>PART FINDER</b></h2></div> 
		    		</div> 
		    		<form action="{{url('/searchresult')}}" method="get" style="padding:30px;" class="finder-form">   
						<div class="row">
							<div class="form-group col-sm-12">  
								<input type="text" id="keyword" class="form-control" placeholder="Keyword ..." name="keyword"  autofocus="">
							</div> 
							<div class="form-group col-sm-12">  
								<select name="make" class="form-control" >
									<option value="honda">&nbsp;Honda</option>
									<option value="kawasaki">&nbsp;Kawasaki</option>
									<option value="yamaha">&nbsp;Yamaha</option>
									<option value="suzuki">&nbsp;Suzuki</option>
								</select>
							</div>  
							<div class="form-group col-sm-12">  
								<input type="text" id="model" class="form-control" placeholder="Model" name="model"  autofocus="">
							</div> 
						</div> 
						<div class="row">
							<div class="form-group col-sm-12 text-right">	
				    		<button class="btn btn-blue" style="padding-left:20px;padding-right:20px;">
				    			<b>SEARCH</b>
				    		</button>
				    		</div>
						</div> 
		    		</form>
		    	</div>
    		</div>
    	</div> 
	</div>
	<div class="col-sm-5 col-sm-push-1 text-right animated fadeIn text-white "> 
		<h1 style="margin-bottom:40px;">QUALITY<br/>MOTORCYLE<br/><b>PARTS SUPPLY</b></h1>
		<p class="text-justify">Alfa Cycle Supply is a leading supplier of Motorcycle Parts and Accessories to companies worldwide. Established in 1983, we are specialist in our field and deliver exactly what our customers want with Quality and Efficiency. We supply OEM after market parts and accessories for Motorcycles, Dirt bikes, ATV and Snow Mobile. We welcome customers from all over the world for any inquiries on parts and custom designs.  </p>
		
		@if(count($catalogs) > 0)
		<div class="row">
			<div class="col-xs-12" style="height:30px;">
			<hr style="width:34%;border-bottom:3px solid #fff;float:right;" /> 
			</div>

			<div class="col-xs-12" style="padding-bottom:30px;">
			<h3 style="font-size:18px;">NEW PRODUCT</h3> 
			</div>
		</div>

		<div class="row">
			<div class="col-md-10 col-md-push-2">
				<div class="row">
				@foreach($catalogs as $catalog) 
					<div class="col-xs-4"> 
		                <a href="{{url('catalog/product/'.$catalog->slug)}}">
							<div class="item-image"><img src="https://s3-ap-southeast-1.amazonaws.com/alfacyclesupply/{{$catalog->name}}.jpg" width="100%" /></div>
							<div class="item-name text-center text-white">{{$catalog->translate(Session::get('locale'))->name}}</div>
						</a>
					</div>
				@endforeach
				</div>
			</div>
		</div>
		<a href="{{url('catalog/newproduct')}}" class="btn btn-transparent" style="margin-top:30px;"> VIEW ALL</a>
		@else
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		@endif
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
	</div>	
	</div>
</div>  
@endsection