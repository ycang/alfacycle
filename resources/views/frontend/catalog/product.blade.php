@extends('partials.frontend.app')

@section('frontend-content')   
 
<div class="page-banner animated fadeIn" style="background-image:url({{asset('/img/img_ProductDetail.jpg')}});"></div> 
<div class="container section-goesup">
 <div class="row breadcrumb">
    <div class="col-xs-12">
      <small><a href="{{url('/')}}">Home</a> / <a href="{{url('/catalog/category/'.$selected_category_catalog[0]->id)}}">{{$selected_category_catalog[0]->translate(Session::get('locale'))->name}}</a>  /<a href="{{Request::url()}}">{{$catalogs[0]->translate(Session::get('locale'))->name}}</a></small>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-3 col-md-3">
    	<p class="font-rockwell text-darkblack" style="font-size:20px;"><b>CATEGORIES</b></p>
      <div class="panel-group" id="accordion"> 
        <div class="panel panel-default">
          <?php echo $categories;?>
        </div> 
      </div>
    </div>

    <div class="col-xs-12 col-sm-9 col-md-9"> 
      <div class="row">
        <div class="col-md-3">
          <img src="https://s3-ap-southeast-1.amazonaws.com/alfacyclesupply/{{$catalogs[0]->name}}.jpg" class="product-image" width="100%" />
        </div>
        <div class="col-md-9"> 
          <p><b>{{$catalogs[0]->translate(Session::get('locale'))->name}}</b></p>
          <p><?php echo nl2br($catalogs[0]->translate(Session::get('locale'))->description);?></p>
          <a href="#" class="btn btn-lg btn-default js-add-to-inquiry__product-view" data-product-id="{{$catalogs[0]->id}}">ADD TO INQUIRY</a>
        </div> 
      </div>
    </div>
  </div>
</div>
@endsection