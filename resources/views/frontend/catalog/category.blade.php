@extends('partials.frontend.app')

@section('frontend-content')   
<div class="page-banner animated fadeIn" style="background-image:url({{asset('/img/img_ProductDetail.jpg')}});"></div>

<div class="container section-goesup">
  <div class="row breadcrumb">
    <div class="col-xs-12">
      <small><a href="{{url('/')}}">Home</a> / <a href="{{Request::url()}}">@if(!$selected_category)
        All Products
        @else 
        {{ $selected_category[0]->translate(Session::get('locale'))->name }}
        @endif</a></small>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-3 col-md-3">
    	<p class="font-rockwell text-darkblack" style="font-size:20px;"><b>CATEGORIES</b></p>
      <div class="panel-group" id="accordion"> 
        <div class="panel panel-default">
          <?php echo $categories;?>
        </div> 
      </div>
    </div>

    <div class="col-xs-12 col-sm-9 col-md-9"> 
        <p class="font-rockwell text-darkblack" style="font-size:18px;border-bottom: 2px dotted #999;padding-bottom:20px;font-weight:bold;">
        @if(!$selected_category)
        All Products
        @else 
        {{ $selected_category[0]->translate(Session::get('locale'))->name }}
        @endif</p> 
    	<div class="items row">
        @if($catalogs)
          @foreach($catalogs as $catalog)
      		<div class="col-xs-6 col-sm-4 col-md-3">  
  		    		<div class="item">
                <div class="item-hover">
                  <div class="item-hover-bg"></div>
                  <div class="item-hover-button-wrapper">
                    <div class="item-button-group">
                      <a href="{{url('catalog/product/'.$catalog->slug)}}" class="btn btn-default btn-view-product">DETAIL</a>
                      <a href="#" class="js-add-to-inquiry btn btn-default" data-product-id="{{$catalog->id}}" style="background-color:#000;padding-top:15px;padding-bottom:15px;"><i class="fa fa-envelope-o text-white"></i> INQUIRY</a>
                    </div>
                  </div> 
                </div>
                <a href="{{url('catalog/product/'.$catalog->slug)}}"> 
      		    			<div class="item-image"><img src="https://s3-ap-southeast-1.amazonaws.com/alfacyclesupply/{{$catalog->name}}.jpg" width="100%"/></div>
      		      		<div class="item-name text-center">{{$catalog->name}}</div>
                </a>
  		    		</div> 
      		</div>  
          @endforeach
        @else
        <div class="col-xs-12"> 
          <p class="text-center" style="padding-top:20px;">No Products.</p>
        </div>  
        @endif 
    	</div>
    </div>
  </div>
</div>
@endsection