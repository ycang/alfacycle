@extends('partials.frontend.app')

@section('frontend-content')
<?php
use App\Models\Status;
use App\Models\Setting;

$settings = Setting::where('status', '=', STATUS::ACTIVE)->get();
$response = Session::get('response');
?>

<div class="page-banner animated fadeIn" style="background-image:url({{asset('/img/img_ContactUs.jpg')}});"></div>
<div class="container section-goesup">
	<div class="row breadcrumb">
	    <div class="col-xs-12">
	      <small><a href="{{url('/')}}">Home</a> / <a href="{{Request::url()}}">Contact Us</a></small>
	    </div>
	  </div>
	  	<div class="row"> 
		<div class="col-md-12"> 
		<h4>Are you making an inquiry? Want to leave a feedback? Need assistance? Our friendly Customer Service Professionals are here to help! Call us!</h4>
		@include('partials.frontend.systemMessage')  
    	</div> 
	</div>
	<div class="row" style="padding-top:20px;">
		<div class="col-md-6"> 
			<div class="contactdetails" style="position:relative;padding-top:40px;">  
				<div style="overflow:hidden;height:320px;width:100%;">
					<div id="map-canvas" style="height:320px;width:100%;"></div>
					<style>#gmap_canvas img{max-width:none!important;background:none!important}</style> 
				</div>
			</div> 
			<div class="row">
				<div class="col-xs-12" style="padding-top:10px;">
					<h4><b>ALFA CYCLE SUPPLY COMPANY</b></h4>
				</div>
			</div>
			<div class="row" style="padding-top:0px;">
				<div class="col-md-6 col-xs-12">
					<div class="media contact-us">
					  <div class="media-left"> 
					  	<i class="fa fa-map-marker"></i>
					  </div>
					  <div class="media-body"> 
					  	<b>Address:</b> 
					  	<p><?php echo $settings->where('code', 'address')->first()->value ;?></p>
					  </div>
					</div>
				</div> 
				<div class="col-md-6 col-xs-12">
					<div class="media">
					  <div class="media-left">  
					  	<i class="fa fa-clock-o"></i>
					  </div>
					  <div class="media-body"> 
					  	<b>Office hours:</b> 
					  	<p>Monday - Friday 8:30am to 5:00pm</p>
					  </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="media">
					  <div class="media-left"> 
					  	<i class="fa fa-envelope"></i> 
					  </div>
					  <div class="media-body"> 
					  	<p><b>Email:</b>
					  	{{ $settings->where('code', 'email')->first()->value }}</p>
					  </div>
					</div>
				</div>
			</div>
			<div class="row"> 
				<div class="col-md-6 col-xs-12">
					<div class="media">
					  <div class="media-left"> 
					  	<i class="fa fa-phone"></i>
					  </div>
					  <div class="media-body"> 
					  	<p><b>Tel:</b> 
					  	<?php echo $settings->where('code', 'phone')->first()->value ;?></p>
					  </div>
					</div>
				</div>
				<div class="col-md-6  col-xs-12">
					<div class="media">
					  <div class="media-left">  
					  	<i class="fa fa-fax"></i> 
					  </div>
					  <div class="media-body">  
					  	<p><b>Fax:</b>
					  	{{ $settings->where('code', 'fax')->first()->value }}</p>
					  </div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6"> 
			<form id="form-contact-us" method="POST" action="{{ url('contact-us/submit') }}" />
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			<input type="hidden" name="referer_path" value="{{url('contact-us')}}" />
			<h4>Inquiry Form</h4>
			<div class="row">
				<div class="form-group col-sm-12">   
					<input type="text" id="inputName" class="form-control" name="name" placeholder="{{ (Session::get('locale') == 'en') ? 'Name*' : '姓名' }}" required="" autofocus="" value="<?php if(Auth::user() && Auth::user()->role =='member'){echo Auth::user()->name;}?>">
				</div>
			</div>

			<div class="row">
				<div class="form-group col-sm-12">  
					<input type="email" id="inputEmail" class="form-control" name="email" placeholder="{{ (Session::get('locale') == 'en') ? 'Email*' : '电邮' }}" required="" autofocus=""  value="<?php if(Auth::user() && Auth::user()->role =='member'){echo Auth::user()->email;}?>">
				</div>
			</div>
				
			<div class="row">
				<div class="form-group col-sm-12">  
					<input type="text" id="inputNumber" class="form-control" name="phone" placeholder="{{ (Session::get('locale') == 'en') ? 'Phone Number*' : '联络号码' }}" required="" autofocus="">
				</div> 
			</div>
			<div class="row">
				<div class="form-group col-sm-12">  
					<input type="text" id="inputSubject" class="form-control" name="subject" placeholder="{{ (Session::get('locale') == 'en') ? 'Subject*' : '标题' }}" required="" autofocus=""  value="">
				</div> 
			</div>   
			<div class="row">
				<div class="form-group col-sm-12">	 
					<textarea name="content" style="width:100%;height:250px;"; required placeholder="{{ (Session::get('locale') == 'en') ? 'Your Message*' : '讯息' }}"></textarea>
				</div>
			</div>
			
			<div class="row"> 
				<div class="form-group col-sm-12 text-right">	 
					<button class="btn btn-lg btn-default" type="submit">{{ (Session::get('locale') == 'en') ? 'SUBMIT' : '呈交' }}</button>
				</div>
		 	</div>	
		</form>
		</div>
	</div>

</div> 
@endsection	
		
@section('frontend-addon-script') 
    <script>
    function initMap() {  
	  var styles = [
	   {
	     featureType: "all",
	     elementType: "all",
	      "stylers": [
		      { "saturation": -100 },
		      { "lightness": 14 },
		      { "gamma": 0.31 } 
		    ] 
	   }
	  ];
	    
	  // Create a new StyledMapType object, passing it the array of styles,
	  // as well as the name to be displayed on the map type control.
	  var styledMap = new google.maps.StyledMapType(styles,
	    {name: "Styled Map"});

	  // Create a map object, and include the MapTypeId to add
	  // to the map type control.
	  var mapOptions = {
	    zoom: 18,
	   	center: {lat: {{ $settings->where('code', 'gmaps_lat')->first()->value }}, lng: {{ $settings->where('code', 'gmaps_lng')->first()->value }} },
	    mapTypeControlOptions: {
	      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
	    }
	  };
	  var map = new google.maps.Map(document.getElementById('map-canvas'),
	    mapOptions);

	  var beachMarker = new google.maps.Marker({
	   position: {lat: {{ $settings->where('code', 'gmaps_lat')->first()->value }}, lng: {{ $settings->where('code', 'gmaps_lng')->first()->value }} },
	   map: map 
	    });
	    

	  //Associate the styled map with the MapTypeId and set it to display.
	  map.mapTypes.set('map_style', styledMap);
	  map.setMapTypeId('map_style');

  	} 
    </script>
  <script src="https://maps.googleapis.com/maps/api/js?callback=initMap"
   async defer></script>

<script type="text/javascript">
function submitForm()
{
	$('#form-contact-us').submit();
}
</script>
@endsection	