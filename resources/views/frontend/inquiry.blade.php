@extends('partials.frontend.app')

@section('frontend-content')
<?php
use App\Models\Status;
use App\Models\Setting; 
$settings = Setting::where('status', '=', STATUS::ACTIVE)->get();
$response = Session::get('response');
?>
<div class="page-banner animated fadeIn" style="background-image:url({{asset('/img/img_InquiryBox.jpg')}});"></div>
<div class="container section-goesup">
	<div class="row breadcrumb">
	    <div class="col-xs-12">
	      <small><a href="{{url('/')}}">Home</a> / <a href="{{Request::url()}}">Inquiry</a></small>
	    </div>
	 </div>
	<div class="row"> 
		<div class="col-md-12"> 
		<h4>Are you making an inquiry? Want to leave a feedback? Need assistance? Our friendly Customer Service Professionals are here to help! Call us!</h4>
		@include('partials.frontend.systemMessage')  
    	</div>
	</div>
	<div class="row" style="padding-top:20px;"> 
		<div class="col-md-12"> 
			<form id="form-contact-us" method="POST" action="{{ url('contact-us/submit') }}" />
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			<input type="hidden" name="referer_path" value="{{url('inquiry')}}" />
			<div class="row">
				<div class="form-group col-sm-6">   
					<input type="text" id="inputName" class="form-control" name="name" placeholder="{{ (Session::get('locale') == 'en') ? 'Name*' : '姓名' }}" required="" autofocus="" value="<?php if(Auth::user() && Auth::user()->role =='member'){echo Auth::user()->name;}?>">
				</div> 
				<div class="form-group col-sm-6">  
					<input type="email" id="inputEmail" class="form-control" name="email" placeholder="{{ (Session::get('locale') == 'en') ? 'Email*' : '电邮' }}" required="" autofocus=""  value="<?php if(Auth::user() && Auth::user()->role =='member'){echo Auth::user()->email;}?>">
				</div>
			</div>
				 
			<div class="row">
				<div class="form-group col-sm-6">  
					<input type="text" id="inputNumber" class="form-control" name="phone" placeholder="{{ (Session::get('locale') == 'en') ? 'Phone Number*' : '联络号码' }}" required="" autofocus="" value="<?php if(Auth::user() && Auth::user()->role =='member'){echo Auth::user()->telephone;}?>">
				</div> 
				<div class="form-group col-sm-6">  
					<input type="text" id="inputSubject" class="form-control" name="subject" placeholder="{{ (Session::get('locale') == 'en') ? 'Subject*' : '标题' }}" required="" autofocus=""  value="">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-12">	 
					<textarea name="content" style="width:100%;height:250px;"; required placeholder="{{ (Session::get('locale') == 'en') ? 'Your Message*' : '讯息' }}">{{$message_append}}</textarea>
				</div>
			</div>
			
			<div class="row"> 
				<div class="form-group col-sm-12 text-right">	 
					<button class="btn btn-lg btn-default" type="submit">{{ (Session::get('locale') == 'en') ? 'SUBMIT' : '呈交' }}</button>
				</div>
		 	</div>	
		</form>
		</div>
	</div>

</div> 
@endsection	 