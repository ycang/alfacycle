<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatalogTranslation extends Model {

	protected $fillable = ['catalog_id', 'locale_id', 'name', 'description'];
}