<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatalogFile extends Model {

	//
	protected $table      = 'catalog_files';
	protected $primaryKey = 'id';
	protected $fillable   = ['catalog_id', 'img_id', 'sort_order', 'status'];

	public function catalog()
	{
		return $this->belongsTo('App\Models\Catalog');
	}

	public function image()
	{
		return $this->belongsTo('App\Models\Files', 'img_id', 'id');
	}
	
}
