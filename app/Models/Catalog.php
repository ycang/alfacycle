<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Translator\Translatable;
use Vinkla\Translator\Contracts\Translatable as TranslatableContract;

class Catalog extends Model implements TranslatableContract {

	//
	use Translatable;

	protected $table                = 'catalogs';
	protected $primaryKey           = 'id';
	protected $fillable             = array(
		'name','description', 'grid_img_id', 'year', 'sort_order', 'status', 'slug',
		'category_id'
	);
	protected $translator           = 'App\Models\CatalogTranslation';
	protected $translatedAttributes = ['name', 'description'];
 
	public function frontImage()
	{
		return $this->hasOne('App\Models\Files', 'id', 'grid_img_id');
	}

	public function catalogImages()
	{
		return $this->hasMany('App\Models\CatalogFile', 'catalog_id', 'id');
	}

	public function translations()
	{
		return $this->hasMany('App\Models\CatalogTranslation', 'catalog_id', 'id');
	}

	public function blocks()
	{
		return $this->hasMany('App\Models\Block', 'catalog_id', 'id')->where('delete', false);
	}

	public function category()
	{
		return $this->hasOne('App\Models\Category', 'id', 'category_id');
	}

}
