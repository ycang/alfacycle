<?php namespace App\Http\Controllers;

use URL;
use Session;
use Redirect;
use Config;
use Validator;
use App\Models\Status;
use App\Models\Locale;
use App\Models\Page;
use App\Models\Category;
use App\Models\Catalog;
use App\Models\Solution;
use App\Models\Message;
use App\Models\Setting;
use Illuminate\Http\Request;
use Cookie;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
	public function __construct()
	{
		// Set default language
		if (Session::get('locale') == null)
		{
			Session::set('locale', 'en');
		}

		return parent::__construct();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getStaticPages($slug)
	{	 
		$slug = ($slug == '/')?$slug:'/'.$slug; 
		$page = Page::where('slug', '=', $slug)->where('status', Status::ACTIVE)
			->where('delete', false)
			->first();   

		if(!$page): 
			$page = Page::where('slug', '=', '/')->where('status', Status::ACTIVE)
			->where('delete', false)
			->first();   
		endif;

		$meta_title = $page->meta_title;
		$meta_keyword = $page->meta_keyword;
		$meta_desc = $page->meta_desc;

		return view('frontend.index')->with([
			'page'=> $page,
			'meta_title'=> $meta_title,
			'meta_keyword'=> $meta_keyword,
			'meta_desc'=> $meta_desc,
			]);
	} 

	public function newproduct()
	{
		//
		$catalogs = Catalog::where('delete', '=', false)->where('status', '=', 2)->orderBy('created_at','desc')->limit(500)->get(); 
		
		return view('frontend.catalog.newproduct')->with('catalogs', $catalogs);
	}

	public function addtoinquiry(Request $request)
	{	  
		if($request->ajax()) { 
			$product_id = $request['product_id'];
			$inquiryproducts = array();
			if(isset($_COOKIE["inquiryproducts"]) && !empty($_COOKIE["inquiryproducts"])){
				$inquiryproducts = $_COOKIE["inquiryproducts"];
				$inquiryproducts .= ','.$product_id; 
				setcookie("inquiryproducts",$inquiryproducts);  
			}else{  
				setcookie("inquiryproducts",$product_id);  
			}
			echo 'yes';
	    } 
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function category($id)
	{  
		$selected_category = '';
		if($id != 'all'):
			$selected_category = Category::where('delete', '=', false)->where('status', '=', '2')->where('id','=',$id)->get();  
			if(!isset($selected_category[0])){ 
				Session::flash('error', 'Category Not Found.'); 
		 		return Redirect::to('/');
			}
		 	$selected_category_all = $this->categoryfindchildids($selected_category[0]->id);
			$selected_category_all .= $selected_category[0]->id; 
			$selected_category_all = explode(',', $selected_category_all);
		endif;   
 
		if($id != 'all'):
			$highlight_cat = $selected_category_all;
			$root_parent_id = $this->categoryfindparentids($selected_category[0]->id); 
			$highlight_cat[] = "$root_parent_id";  
			$categories = $this->categoryfindchildtoHtml($id,0,$highlight_cat); 
		else:
			$categories = $this->categoryfindchildtoHtml();  
		endif;  
 

		if($id != 'all'):
			$catalogs = Catalog::where('delete', '=', false)->where('status', '=', '2')->whereIn('category_id',$selected_category_all)->orderBy('sort_order')->get(); 
		else:
			$catalogs = Catalog::where('delete', '=', false)->where('status', '=', '2')->orderBy('sort_order')->get(); 
		endif;

		return view('frontend.catalog.category')
			->with(['catalogs'		=> $catalogs,
				'selected_category'	=> $selected_category,
				'categories'		=> $categories,
				'meta_title'=> $selected_category[0]->translate(Session::get('locale'))->name,
				'meta_keyword'=> $selected_category[0]->translate(Session::get('locale'))->name,
				'meta_desc'=> $selected_category[0]->translate(Session::get('locale'))->name,]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function product($slug)
	{
		$catalogs = Catalog::where('delete', '=', false)->where('status', '=', '2')->where('slug', '=', $slug)->get();  
		if(!isset($catalogs[0])): 
			Session::flash('error', 'Product Not Found.'); 
		 	return Redirect::to('/');
		endif; 

		$selected_category = $catalogs[0]->category_id;
	 	$selected_category_all = $this->categoryfindchildids($selected_category);
		$selected_category_all .= $selected_category; 
		$selected_category_all = explode(',', $selected_category_all);
	  
 		$root_parent_id = $this->categoryfindparentids($selected_category);
		$highlight_cat = $selected_category_all;
		$highlight_cat[] = "$root_parent_id"; 
		$categories = $this->categoryfindchildtoHtml(0,0,$highlight_cat);  
		
		$selected_category_catalog =  Category::where('id', '=', $catalogs[0]->category_id)->get();
 
		return view('frontend.catalog.product')
			->with(['catalogs'		=> $catalogs,
				'selected_category'	=> $selected_category,
				'categories'		=> $categories,
				'selected_category_catalog'=>$selected_category_catalog,
				'meta_title'=> $catalogs[0]->translate(Session::get('locale'))->name,
				'meta_keyword'=> $catalogs[0]->translate(Session::get('locale'))->name,
				'meta_desc'=> $catalogs[0]->translate(Session::get('locale'))->name]);
	}


	public function categoryfindparentids($selected_id = null){ 
	 
		$result = Category::where('delete', '=', false)->where('status', '=', '2')->where('id','=',$selected_id)->get(); 
		if(isset($result[0]) && $result[0]->parent_id != 0):
			$parent_id = $result[0]->parent_id;
			if($parent_id != 0){
				$root_parent = $this->categoryfindparentids($parent_id);
			}else{
				$root_parent = $result[0]->id;
			} 
		else:
			$root_parent = $selected_id;
		endif; 
		return $root_parent; 
	} 

	public function categoryfindchildids($parent_id = 0,$level = 0,$selected_id = null){ 
		if(!isset($record_option_html)){  
			$record_option_html = ''; 
		}   
		$categories = Category::where('delete', '=', false)->where('status', '=', '2')->where('parent_id','=',$parent_id)->get();  
		if(count($categories) > 0):
			$count = 0; 
			$level++;
			foreach ($categories as $key => $category) {   
				$record_option_html .= $category->id.',';
				$record_option_html .= $this->categoryfindchildids($category->id,$level,$selected_id);
			 	 
			 	$count ++;
			}
		endif;
		return $record_option_html;
	} 

	public function categoryfindchildtoHtml($parent_id = 0,$level = 0,$selected_id = null){ 
		if(!isset($record_option_html)){  
			$record_option_html = ''; 
		}   
		$categories = Category::where('delete', '=', false)->where('status', '=', '2')->where('parent_id','=',$parent_id)->get();  
		 
			if(count($categories) > 0):
				$count = 0; 
				$level++;
				foreach ($categories as $key => $category) {    
						$record_option_html .= '<a href="'.url('catalog/category/'.$category->id).'" class="list-group-item';
						$record_option_html .='">'; 
						for($level_count = 0 ; $level_count < $level; $level_count++){ 
							$record_option_html .= '&nbsp;&nbsp;';
						} 
				 	if($level == 1){
				 		$record_option_html .='<b class="text-darkblack">';
				 	}
						$record_option_html .=$category->translate(Session::get('locale'))->name.'</a>';
					if($level == 1){
				 		$record_option_html .='</b>';
				 	}
					$record_option_html .= $this->categoryfindchildtoHtml($category->id,$level,$selected_id); 
				 	 
				 	$count ++;
				}
			endif;
		return $record_option_html;
	} 
 
	public function getIndex()
	{ 
		$meta_title = 'Alfa Cycle Supply';
		$meta_keyword = 'Alfa Cycle Supply';
		$meta_desc = 'Alfa Cycle Supply';

		$catalogs = Catalog::where('status','2')->orderBy('created_at', 'desc')->take(3)->get(); 

		return view('frontend.index')->with([ 
			'meta_title'=> $meta_title,
			'meta_keyword'=> $meta_keyword,
			'meta_desc'=> $meta_desc,
			'catalogs'=> $catalogs,
			]);
	}

	public function searchresult()
	{ 
		$meta_title = 'Search Result';
		$meta_keyword = 'Search Result';
		$meta_desc = 'Search Result.'; 
		$keyword = (isset($_REQUEST['keyword']))?$_REQUEST['keyword']:'';
		$category = (isset($_REQUEST['category']))?$_REQUEST['category']:''; 
		$make = (isset($_REQUEST['make']))?$_REQUEST['make']:'';
		$model = (isset($_REQUEST['model']))?$_REQUEST['model']:'';
		if(empty($keyword)):
			$catalogs = '';
		else: 
			$catalogs =  Catalog::leftJoin('catalog_translations', function($join) {
	      				$join->on('catalogs.id', '=', 'catalog_translations.catalog_id');
	    				})->where('name','LIKE', '%'.$keyword.'%')->orWhere('description','like', '%'.$keyword.'%'); 

			if(!empty($make)):
			   	$catalogs->orWhere('name','LIKE', '%'.$make.'%')->orWhere('description','like', '%'.$make.'%');
			endif; 

			if(!empty($model)): 
			    $catalogs->orWhere('name','LIKE', '%'.$model.'%')->orWhere('description','like', '%'.$model.'%');
			endif; 
			$catalogs = $catalogs->get(); 
		endif;

		if($category): 
			$child_ids = $this->categoryfindchildids($category);
			$child_ids_arr = explode(',', $child_ids);
			// remove last empty element
			$empty_element = array_pop($child_ids_arr); 
			if(!empty($child_ids_arr) && $child_ids_arr[0] != ''):
				$counter = 0;
				foreach ($catalogs as $catalog) {
					# code... 
					if(!in_array($catalog->id,$child_ids_arr)):
						unset($catalogs[$counter]); 
					endif;
					$counter++;
				} 
			else: 
				$catalogs = array(); 
			endif;
		endif;

		return view('frontend.searchresult')->with([ 
			'meta_title'=> $meta_title,
			'meta_keyword'=> $meta_keyword,
			'meta_desc'	=> $meta_desc,
			'catalogs'	=> $catalogs,
			'keyword'	=> $keyword
			]);
	}  

	public function getInquiry()
	{
		$meta_title = 'Inquiry';
		$meta_keyword = 'Inquiry';
		$meta_desc = 'Inquiry.';	
		$message_append = '';   
		if(isset($_COOKIE["inquiryproducts"])):
			$inquiryproducts_arr = explode(',', $_COOKIE["inquiryproducts"]);  
			$inquiryproducts_arr = array_unique($inquiryproducts_arr);  
			$new_arr = implode(',', $inquiryproducts_arr);
			foreach ($inquiryproducts_arr as $key => $product_id) { 
				if($product_id != ''):  
					$catalog = Catalog::find($product_id); 
					$message_append .= 'Product : '.$catalog->translate(Session::get('locale'))->name.' &#013;Description : '.$catalog->translate(Session::get('locale'))->description.'&#013;Remarks : Write your remarks here ... &#013; &#010;'; 
				endif;
			}
		endif;
		  
		return view('frontend.inquiry')->with([ 
			'meta_title'=> $meta_title,
			'meta_keyword'=> $meta_keyword,
			'meta_desc'=> $meta_desc,
			'message_append'=>$message_append
			]);
	}

	public function getContactUs()
	{
		$meta_title = 'Contact Us';
		$meta_keyword = 'Contact Us';
		$meta_desc = 'Contact Us.';

		return view('frontend.contactus')->with([ 
			'meta_title'=> $meta_title,
			'meta_keyword'=> $meta_keyword,
			'meta_desc'=> $meta_desc,
			]);
	}

	public function switchLocale($code)
	{
		if(empty($code)):
			return Redirect::to('/');
		endif;

		$locale = Locale::where('language', '=', $code)->where('status', Status::ACTIVE)
			->where('delete', false)
			->first();

		if (isset($locale))
		{
			Session::set('locale', $locale->language);
		}

		return Redirect::back();
	}

	public function submitMessage(Request $req)
	{
		$response = array();
		$data     = $req->input(); 
		if (isset($data) && !empty($data))
		{
			$rules = array(
				'name'    => 'required',
				'phone'   => 'required',
				'email'   => 'required',
				'subject' => 'required',
				'content' => 'required',
			);

			$validator = Validator::make($data, $rules);

			if ($validator->fails())
			{
				$msg = array();
				$messages = $validator->messages();
				foreach ($messages->all() as $message)
				{
					$msg[] = $message;
				}
				$response['code'] = Status::ERROR;
				$response['msg']  = implode(" | ", $msg);
			}
			else
			{
				$message = array();
				$message['name']    = $data['name'];
				$message['phone']   = $data['phone'];
				$message['email']   = $data['email'];
				$message['subject'] = $data['subject'];
				$message['content'] = $data['content'];
				$return = Message::create($message);

				$email   = Setting::where('code', '=', 'email')->first();
				$content = "FROM: ".$data['email']
					."\nNAME: ".$data['name']
					."\nPhone: ".$data['phone']
					."\nSubject: ".$data['subject']
					."\nContent: ".$data['content'];
				$emailHeader = "From: yeowchuan89@gmail.com\n"
				   . "MIME-Version: 1.0\n"
				   . "Content-type: text/plain; charset=\"UTF-8\"\n"
				   . "Content-transfer-encoding: 8bit\n";

				if (isset($email) && isset($email->value) && $email->value != '')
				{
					$result = mail($email->value, 'Alfa Cycle Supply Enquiry', $content,$emailHeader);
					Session::flash('success', 'Thank You for contacting us. We\'ll reply you within 2 working days'); 
				}
			}
		}
		else
		{ 
			Session::flash('error', 'Invalid parameters.'); 
		}

		return Redirect::back()->with('response', $response);
	}

}
