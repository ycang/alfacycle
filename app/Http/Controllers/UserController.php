<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use Redirect;
use DateTime;
use App\Models\Status;

class UserController extends Controller {

	public function loginPath(){

		return url('/admin');
	}
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		if(isset($_REQUEST['show']) && $_REQUEST['show'] = 'all'):
			$users = User::where('role','!=','admin')->orderBy('created_at','desc')->get(); 
		else:
			$users = User::where('role','!=','admin')->where('status','1')->orderBy('created_at','desc')->get(); 
		endif;

		return view('management.user.index')->with('users', $users);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

		/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function setInactive($id)
	{
		//
		$response = array();
		$user     = User::find($id); 

		if (isset($user) && isset($user->id))
		{
			$user->status = Status::INACTIVE;
			$user->save();

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Member [#".$user->email."] is deactivated successfully.";
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = "Member not found.";
		}

		return Redirect::to('admin/manage/user')->with('response', $response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function setActive($id)
	{
		//
		$response = array();
		$user     = User::find($id);

		if (isset($user) && isset($user->id))
		{
			$user->status = Status::ACTIVE;
			$user->save();

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Member [#".$user->email."] is activated successfully.";
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = "Member not found.";
		}

		return Redirect::to('admin/manage/user')->with('response', $response);
	}
}
