<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Redirect;
use Excel;
use File;
use Session;
use App\Models\Status;
use App\Models\Locale;
use App\Models\Block;
use App\Models\Catalog;
use App\Models\CatalogTranslation;
use App\Models\CatalogFile;
use App\Models\Category;

use App\Services\FileHelper;
use App\Services\ValidationHelper;

class CatalogController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$catalogs = Catalog::where('delete', '=', false)->orderBy('sort_order')->get();
		$categories = Category::where('delete', '=', false)->get();
		
		return view('management.catalog.index')->with('catalogs', $catalogs)
			->with('categories', $categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$categories_treeview = array();  
		$categories_treeview_option = $this->categoryfindchildtoOption();  
		return view('management.catalog.create')->with('categories', $categories_treeview_option);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function import($category_id)
	{
		//
		$categories_treeview = array();   
		return view('management.catalog.import')->with('category_id', $category_id);
	} 

	public function massdelete(Request $req)
	{
		// 
		$data         = $req->input(); 
		$counter = 0 ;
		if(isset($data['id'])):
			foreach ($data['id'] as $key => $id) {
				# code...
				$catalog  = Catalog::find($id);

				if (isset($catalog) && isset($catalog->id))
				{
					$catalog->delete = true;
					$catalog->save();
					$counter++; 
				}
			} 

			$response['code'] = Status::SUCCESS;
			$response['msg']  = $counter." record(s) has been deleted successfully.";
		else:  
			$response['code'] = Status::ERROR;
			$response['msg']  = "No record found.";
		endif; 
		return Redirect::to('admin/manage/catalog')->with('response', $response);
	} 

	public function storeimport(Request $req,FileHelper $fileHelper)
	{
		$files = $req->file();  
		$category_id = $req->category_id; 
		$total = 0;
		if (isset($files) && !empty($files))
		{    
			$fileName = 'excelfile.xlsx';
			$ismoved = $files['file']->move(public_path().'/', $fileName);  
			$counter = 0 ;
			$response['code'] = Status::ERROR; 	
			$response['msg']  = 'No item has been imported.';
			$result = Excel::filter('chunk')->load($fileName)->chunk(99999,function($reader) use ($category_id,$total,&$counter,&$response) {
				$results = $reader->toArray();    
				foreach ($results as $key => $value) {
					# code...   
					foreach ($value as $k => $v) { 
						$content = $v; 
						if($content): 
							try
	            			{
								$description = $content;
								$linebreak = explode(PHP_EOL,$content); 
								$name = $linebreak[0]; 
								$name = str_replace('Item No. ', "", $name);   
								$catalogData = array();  
								$catalogData['category_id']            = $category_id;  
								$catalogData['slug']                   = $name;
								$catalogData['name']                   = $name;
								$catalogData['description']            = $description; 
								$catalog = Catalog::create($catalogData); 
								$counter++;
							}catch(\Illuminate\Database\QueryException $e)
							{
								$response['code'] = Status::ERROR; 	
								$response['msg']  = "Error on item : \"".$name."\", Error Message: ".$e->errorInfo[2];
					            return Redirect::back()->with('response', $response); 
							}
						endif; 
					} 
				}  
			});
			if($counter > 0){ 
				$response['code'] = Status::SUCCESS; 	
				$response['msg']  = $counter." items have been imported."; 
			}else{ 
				$response['code'] = (isset($response['code']))?$response['code']:Status::ERROR;
				$response['msg']  = (isset($response['msg']))?$response['msg']:"No item has been imported. Please check your format.";
			}
		}else{ 
			$response['code'] = Status::ERROR;
			$response['msg']  = "No item has been imported.";
		}
		return Redirect::back()->with('response', $response);

	}

	public function categoryfindchildtoOption($parent_id = 0,$level = 0,$selected_id = null){ 
		if(!isset($record_option_html)){  
			$record_option_html = ''; 
		}   
		$categories = Category::where('delete', '=', false)->where('parent_id','=',$parent_id)->get();  
	
			if(count($categories) > 0):
				$count = 0; 
				$level++;
				foreach ($categories as $key => $category) {   
					$record_option_html .= '<option value="'.$category->id.'"';
					if($category->id == $selected_id):
					$record_option_html .= 'selected="selected"';
					endif;
					$record_option_html .= '>';
					if($level > 1){
						for($level_count = 0 ; $level_count < $level; $level_count++){ 
							$record_option_html .= '&nbsp;';
						} 
					}
					$record_option_html .= $category->translate(Session::get('locale'))->name.'</option>';
					$record_option_html .= $this->categoryfindchildtoOption($category->id,$level,$selected_id);
				 	$count ++;
				}
			endif;
		return $record_option_html;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $req, FileHelper $fileHelper, ValidationHelper $validator)
	{
		//
		$response     = array();
		$data         = $req->input();
		$gridImgId    = '';
		$gridBgImgId  = '';
		$brandImgId   = '';
		$mascottImgId = '';
		$videoImgId   = '';
		$newImgIds    = array();
		$thumbnails   = array();

		if (isset($data) && !empty($data))
		{
			$fields = array('slug');

			if ($result = $validator->validateRequired($fields, $data))
			{
				$response['code'] = Status::ERROR;
				$response['msg']  = $result;

				return Redirect::back()->with('response', $response);
			}
			else if (Catalog::where('slug', '=', $data['slug'])->where('status', Status::ACTIVE)->get()->count() > 0)
			{
				$response['code'] = Status::ERROR;
				$response['msg']  = "URL key already exist.";

				return Redirect::back()->with('response', $response);
			}
			else
			{
				$files = $req->file();

				if (isset($files) && !empty($files))
				{
					foreach ($files as $key => $val)
					{
						if ($key == 'new_grid_img_id')
						{
							$gridImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_grid_bg_img_id')
						{
							$gridBgImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_brand_img_id')
						{
							$brandImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_mascott_img_id')
						{
							$mascottImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_video_img_id')
						{
							$videoImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_catalog_img_id')
						{
							foreach ($val as $id => $img)
							{
								if (is_array($img) && !empty($img))
								{
									$imgs = array();
									foreach ($img as $k => $v)
									{
										if (isset($v))
										{
											$imgs[] = $fileHelper->uploadNewFile($v);
										}
									}

									$newImgIds[$id] = implode(",", $imgs);
								}
							}
						}
						else if ($key == 'new_thumbnail')
						{
							foreach ($val as $id => $img)
							{
								if (is_array($img) && !empty($img))
								{
									$imgs = array();
									foreach ($img as $k => $v)
									{
										if (isset($v))
										{
											$imgs[] = $fileHelper->uploadNewFile($v);
										}
									}

									$thumbnails[$id] = implode(",", $imgs);
								}
							}
						}
					}
				}

				$catalogData = array(); 
				$catalogData = $this->_saveCatalogImg($gridImgId, $data, 'grid_img_id', $catalogData); 

				$catalogData['category_id']            = $data['category_id']; 
				$catalogData['year']                   = $data['year'];
				$catalogData['slug']                   = str_replace(" ", "-", $data['slug']);
				$catalogData['sort_order']             = $data['sort_order'];
				$catalog = Catalog::create($catalogData);

				$projData   = array();
				$attributes = array('name', 'description');
				$locales    = Locale::where('status', '=', Status::ACTIVE)->get();

				foreach ($locales as $locale)
				{
					$projData['catalog_id'] = $catalog->id;
					$projData['locale_id']  = $locale->id;

					foreach ($attributes as $attribute)
					{
						if (isset($data[$attribute][$locale->id]))
						{
							$projData[$attribute] = $data[$attribute][$locale->id];
						}
					}

					CatalogTranslation::create($projData);
				}
 
				$response['code'] = Status::SUCCESS;
				$response['msg']  = "Catalog [#".$catalog->id."] has been created successfully.";

				return Redirect::to('admin/manage/catalog')->with('response', $response);	
			}		
		}

		$response['code'] = Status::ERROR;
		$response['msg']  = "Unable to create new catalog.";

		return Redirect::back()->with('response', $response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$response = array();
		$catalog  = Catalog::where('id', '=', $id)->where('delete', false)->first();

		$categories_treeview = array();  
		$categories_treeview_option = $this->categoryfindchildtoOption('0','0',$catalog->category_id);   
		if (isset($catalog) && isset($catalog->id))
		{
			return view('management.catalog.edit')->with(['catalog'=> $catalog,'categories'=> $categories_treeview_option]);
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = 'Catalog not found.';
			
			return Redirect::back()->with('response', $response);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $req, FileHelper $fileHelper, ValidationHelper $validator)
	{
		//
		$response     = array();
		$data         = $req->input();
		$catalog      = Catalog::find($id);
		$gridImgId    = '';
		$gridBgImgId  = '';
		$brandImgId   = '';
		$mascottImgId = '';
		$videoImgId   = '';
		$newImgIds    = array();
		$thumbnails   = array();

		if (isset($data) && $catalog->id)
		{
			$fields = array('slug');

			if ($result = $validator->validateRequired($fields, $data))
			{
				$response['code'] = Status::ERROR;
				$response['msg']  = $result;

				return Redirect::back()->with('response', $response);
			}
			else if (Catalog::where('slug', '=', $data['slug'])->where('status', Status::ACTIVE)->where('delete', false)->where('id','!=',$id)->get()->count() > 0)
			{
				$response['code'] = Status::ERROR;
				$response['msg']  = "URL key already exist.";

				return Redirect::back()->with('response', $response);
			}
			else
			{
				$files = $req->file();

				if (isset($files) && !empty($files))
				{
					foreach ($files as $key => $val)
					{
						if ($key == 'new_grid_img_id')
						{
							$gridImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_grid_bg_img_id')
						{
							$gridBgImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_brand_img_id')
						{
							$brandImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_mascott_img_id')
						{
							$mascottImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_video_img_id')
						{
							$videoImgId = $fileHelper->uploadNewFile($val);
						}
						else if ($key == 'new_catalog_img_id')
						{
							foreach ($val as $id => $img)
							{
								if (is_array($img) && !empty($img))
								{
									$imgs = array();
									foreach ($img as $k => $v)
									{
										if (isset($v))
										{
											$imgs[] = $fileHelper->uploadNewFile($v);
										}
									}

									$newImgIds[$id] = implode(",", $imgs);
								}
							}
						}
						else if ($key == 'new_thumbnail')
						{
							foreach ($val as $id => $img)
							{
								if (is_array($img) && !empty($img))
								{
									$imgs = array();
									foreach ($img as $k => $v)
									{
										if (isset($v))
										{
											$imgs[] = $fileHelper->uploadNewFile($v);
										}
									}

									$thumbnails[$id] = implode(",", $imgs);
								}
							}
						}
					}
				}

				$catalogData = array();
				$catalogData = $this->_saveCatalogImg($gridImgId, $data, 'grid_img_id', $catalogData); 

				$catalogData['category_id']            = $data['category_id'];
				$catalogData['year']                   = $data['year'];
				$catalogData['slug']                   = str_replace(" ", "-", $data['slug']);
				$catalogData['sort_order']             = $data['sort_order'];
				$catalog->update($catalogData);

				$projData   = array();
				$attributes = array('name', 'description');
				$locales    = Locale::where('status', '=', Status::ACTIVE)->get();

				foreach ($locales as $locale)
				{
					$projData['catalog_id'] = $catalog->id;
					$projData['locale_id']  = $locale->id;

					foreach ($attributes as $attribute)
					{
						if (isset($data[$attribute][$locale->id]))
						{
							$projData[$attribute] = $data[$attribute][$locale->id];
						}
					}

					$projTranslation = $catalog->translations()
						->where('locale_id', $locale->id)
						->first();

					if (isset($projTranslation))
					{ 
						$projTranslation->update($projData);
					}
					else
					{
						CatalogTranslation::create($projData);
					}
				}
 
				$response['code'] = Status::SUCCESS;
				$response['msg']  = "Catalog [#".$catalog->id."] has been updated successfully.";

				return Redirect::to('admin/manage/catalog')->with('response', $response);
			}
		}

		$response['code'] = Status::ERROR;
		$response['msg']  = "Unable to update catalog.";

		return Redirect::back()->with('response', $response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$response = array();
		$catalog  = Catalog::find($id);

		if (isset($catalog) && isset($catalog->id))
		{
			$catalog->delete = true;
			$catalog->save();

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Catalog [#".$catalog->id."] has been deleted successfully.";
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = "Catalog not found.";
		}

		return Redirect::to('admin/manage/catalog')->with('response', $response);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function setInactive($id)
	{
		//
		$response = array();
		$catalog  = Catalog::find($id); 

		if (isset($catalog) && isset($catalog->id))
		{
			$catalog->status = 1;
			$catalog->save();

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Catalog [#".$catalog->id."] is deactivated successfully.";
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = "Catalog not found.";
		}

		return Redirect::to('admin/manage/catalog')->with('response', $response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function setActive($id)
	{
		//
		$response = array();
		$catalog  = Catalog::find($id);

		if (isset($catalog) && isset($catalog->id))
		{
			$catalog->status = 2;
			$catalog->save();

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Catalog [#".$catalog->id."] is activated successfully.";
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = "Catalog not found.";
		}

		return Redirect::to('admin/manage/catalog')->with('response', $response);
	}

	protected function _saveCatalogImg($img, $input, $key, $catalogData)
	{
		if (is_array($catalogData))
		{
			if (isset($img) && $img != '')
			{
				$catalogData[$key] = $img;
			}
			else if (isset($input[$key]))
			{
				if ($input[$key] != '')
				{
					$catalogData[$key] = $input[$key];
				}
				else
				{
					$catalogData[$key] = null;
				}
			}

			return $catalogData;
		}
	}

}
