<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Redirect;
use Validator;
use Session;
use App\Models\Status;
use App\Models\Locale;
use App\Models\Category;
use App\Models\CategoryTranslation;

use App\Services\FileHelper;
use App\Services\ValidationHelper;

class CategoryController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$categories = $this->categoryfindchildtoHtml(); 
		// dd($categories);
		return view('management.category.index')->with('categories', $categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// 
		$categories_treeview = array();  
		$categories_treeview_option = $this->categoryfindchildtoOption();    
		return view('management.category.create')->with('categories', $categories_treeview_option);
	}

	public function categoryfindchildtoHtml($parent_id = 0,$level = 0,$selected_id = null){ 
		if(!isset($record_option_html)){  
			$record_option_html = ''; 
		}  
		if($selected_id): 
			$categories = Category::where('delete', '=', false)->where('parent_id','=',$parent_id)->where('id','!=',$selected_id)->get();  
		else:
			$categories = Category::where('delete', '=', false)->where('parent_id','=',$parent_id)->get();  
		endif;
			if(count($categories) > 0):
				$count = 0; 
				$level++;
				foreach ($categories as $key => $category) {   
					if($level > 1){ 
						$record_option_html .= '<ul>'; 
						for($level_count = 0 ; $level_count < $level; $level_count++){ 
							$record_option_html .= '&nbsp;';
						}  
					}
					$record_option_html .= '<li class="treeview';
					if($category->status != '2'){
						$record_option_html .= ' text-danger';
					}
					$record_option_html .= ' ">'; 
					$record_option_html .= $category->translate(Session::get('locale'))->name.' [ <a href="'.url('admin/manage/catalog/import/' . $category->id).'" target="_blank">Import Catalog</a> | <a href="'.url('admin/manage/category/edit/'. $category->id).'" target="_blank">Edit</a> | ';
					if($category->status == '1'){
						$record_option_html .= '<a href="'.url('admin/manage/category/setActive/' . $category->id).'">Set Active</a>';
					}elseif($category->status == '2'){
						$record_option_html .= '<a href="'.url('admin/manage/category/setInactive/' . $category->id).'">Set Inactive</a>';
					}
					$record_option_html .=' | <a href="#" class="js-collapse">Toggle View</a>';
					$record_option_html .=']';
					$record_option_html .= $this->categoryfindchildtoHtml($category->id,$level,$selected_id);
				 	
					$record_option_html .= '</li>'; 
					if($level > 1){ 
						$record_option_html .= '</ul>'; 
					}

				 	$count ++;
				}
			endif;
		return $record_option_html;
	}

	public function categoryfindchildtoOption($parent_id = 0,$level = 0,$selected_id = null){ 
		if(!isset($record_option_html)){  
			$record_option_html = ''; 
		}  
		if($selected_id): 
			$categories = Category::where('delete', '=', false)->where('parent_id','=',$parent_id)->where('id','!=',$selected_id)->get();  
		else:
			$categories = Category::where('delete', '=', false)->where('parent_id','=',$parent_id)->get();  
		endif;
			if(count($categories) > 0):
				$count = 0; 
				$level++;
				foreach ($categories as $key => $category) {  
					$record_option_html .= '<option value="'.$category->id.'">';
					if($level > 1){
						for($level_count = 0 ; $level_count < $level; $level_count++){ 
							$record_option_html .= '&nbsp;';
						} 
					}
					$record_option_html .= $category->translate(Session::get('locale'))->name.'</option>';
					$record_option_html .= $this->categoryfindchildtoOption($category->id,$level,$selected_id);
				 	$count ++;
				}
			endif;
		return $record_option_html;
	}

	public function categoryfindchild($parent_id = 0,$level = 0){
		$record = array(); 
		$record['option_html'] = '';
		$categories = Category::where('delete', '=', false)->where('parent_id','=',$parent_id)->get();  
			if(count($categories) > 0):
				$count = 0; 
				$level++;
				foreach ($categories as $key => $category) {  
					$record[$count]['object'] = $category;
					$record[$count]['level'] = $level;
				 	$record[$count]['child'] =$this->categoryfindchild($category->id,$level);
				 	$count ++;
				}
			endif;
		return $record;
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $req, FileHelper $fileHelper, ValidationHelper $validator)
	{
		$response = array();
		$data     = $req->input(); 
		if (isset($data) && !empty($data))
		{ 
			$category = new Category;  
			$category->sort_order = $data['sort_order'];
			$category->parent_id = $data['parent_id'];
			$category->save();

			$catData    = array();
			$attributes = array('name');
			$locales 	= Locale::where('status', '=', Status::ACTIVE)->get();

			foreach ($locales as $locale)
			{
				$catData['category_id'] = $category->id;
				$catData['locale_id']   = $locale->id;

				foreach ($attributes as $attribute)
				{
					if (isset($data[$attribute][$locale->id]))
					{
						$catData[$attribute] = $data[$attribute][$locale->id];
					}
				}

				CategoryTranslation::create($catData);
			}

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Category [#".$category->id."] has been created successfully.";

			return Redirect::to('admin/manage/category')->with('response', $response);
		}

		$response['code'] = Status::ERROR;
		$response['msg']  = "Unable to save new category.";

		return Redirect::back()->with('response', $response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$response = array();
		$category = Category::where('id', '=', $id)->where('delete', false)->first();
		$categories_treeview = array();  
		$categories_treeview_option = $this->categoryfindchildtoOption(0,0,$category->id);       
		if (isset($category) && isset($category->id))
		{
			return view('management.category.edit')->with(['category'=> $category,'categories'=> $categories_treeview_option]);
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = 'Category not found.';
			
			return Redirect::back()->with('response', $response);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $req, FileHelper $fileHelper, ValidationHelper $validator)
	{
		//
		$response = array();
		$data     = $req->input();
		$category = Category::find($id);

		if (isset($data) && $category->id)
		{ 
			$category->sort_order = $data['sort_order'];
			$category->parent_id = $data['parent_id'];
			$category->save();

			$catData    = array();
			$attributes = array('name');
			$locales 	= Locale::where('status', '=', Status::ACTIVE)->get();

			foreach ($locales as $locale)
			{
				$catData['category_id'] = $category->id;
				$catData['locale_id']   = $locale->id;

				foreach ($attributes as $attribute)
				{
					if (isset($data[$attribute][$locale->id]))
					{
						$catData[$attribute] = $data[$attribute][$locale->id];
					}
				}

				$catTranslation = $category->translations()
					->where('locale_id', $locale->id)
					->first();

				if (isset($catTranslation))
				{
					$catTranslation->update($catData);
				}
				else
				{
					CategoryTranslation::create($catData);
				}
			}

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Category [#".$category->id."] has been updated successfully.";

			return Redirect::to('admin/manage/category')->with('response', $response);
		 
		}

		$response['code'] = Status::ERROR;
		$response['msg']  = "Unable to update category.";

		return Redirect::back()->with('response', $response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$response = array();
		$category = Category::find($id);

		if (isset($category) && isset($category->id))
		{
			$category->delete = true;
			$category->save();

			$response['code'] = Status::SUCCESS;
			$response['msg'] = "Category [#".$category->id."] has been deleted successfully.";
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg'] = "Category not found.";
		}

		return Redirect::to('admin/manage/category')->with('response', $response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function setInactive($id)
	{
		//
		$response = array();
		$category = Category::find($id); 

		if (isset($category) && isset($category->id))
		{
			$category->status = Status::INACTIVE;
			$category->save();

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Category [#".$category->id."] is deactivated successfully.";
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = "Category not found.";
		}

		return Redirect::to('admin/manage/category')->with('response', $response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function setActive($id)
	{
		//
		$response = array();
		$category = Category::find($id);

		if (isset($category) && isset($category->id))
		{
			$category->status = Status::ACTIVE;
			$category->save();

			$response['code'] = Status::SUCCESS;
			$response['msg']  = "Category [#".$category->id."] is activated successfully.";
		}
		else
		{
			$response['code'] = Status::ERROR;
			$response['msg']  = "Category not found.";
		}

		return Redirect::to('admin/manage/category')->with('response', $response);
	}

}
