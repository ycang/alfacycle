<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;

use App\User;
use Session;
use App\Services\MemberRegistrar;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class MemberController extends Controller {

	/**
	 * The Guard implementation.
	 *
	 * @var \Illuminate\Contracts\Auth\Guard
	 */
	protected $auth;  
 

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getRegister()
	{
		$meta_title = 'Member Sign Up';
		$meta_keyword = 'Member Sign Up';
		$meta_desc = 'Member Sign Up';

		return view('frontend.member.register')->with([ 
			'meta_title'=> $meta_title,
			'meta_keyword'=> $meta_keyword,
			'meta_desc'=> $meta_desc,
			]);;
	}

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postRegister(Request $request,MemberRegistrar $memberRegistrar)
	{ 
		$validator = $memberRegistrar->validator($request->all());  
		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		} 

		$memberRegistrar->create($request->all());

		Session::flash('success', 'Congratulations! You have successfully sign up. Admin will review your account soon.'); 
		return redirect($this->loginPath());
	}

	/**
	 * Show the application login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin()
	{
		$meta_title = 'Member Login';
		$meta_keyword = 'Member Login';
		$meta_desc = 'Member Login';

		return view('frontend.member.login')->with([ 
			'meta_title'=> $meta_title,
			'meta_keyword'=> $meta_keyword,
			'meta_desc'=> $meta_desc,
			]);;
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email', 'password' => 'required'
		]);

		$user = User::where('email', '=', $request['email'])->first();
		if($user){
			if($user->status == 1){ 
				return redirect($this->loginPath())
						->withInput($request->only('email', 'remember'))
						->withErrors([
							'email' => 'Your account is under review.',
						]);
			}
		}

		$request['role'] = 'member';
		$request['status'] = '2';
		$credentials = $request->only('email', 'password', 'role', 'status');

		if ($result = Auth::attempt($credentials, $request->has('remember')))
		{
			return redirect()->intended($this->redirectPath());
		} 

		return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
					]);
	}

	/**
	 * Get the failed login message.
	 *
	 * @return string
	 */
	protected function getFailedLoginMessage()
	{
		return 'These credentials do not match our records.';
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout()
	{
		setcookie("inquiryproducts", "", time() - 3600);
		
		Auth::logout(); 

		return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
	}

	/**
	 * Get the post register / login redirect path.
	 *
	 * @return string
	 */
	public function redirectPath()
	{
		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}

		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
	}

	/**
	 * Get the path to the login route.
	 *
	 * @return string
	 */
	public function loginPath()
	{
		return property_exists($this, 'loginPath') ? $this->loginPath : '/member/login';
	}

}
