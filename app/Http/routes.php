<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
 

// Password reset link request routes...
Route::get('/member/password/email', 'MemberPasswordController@getEmail');
Route::post('/member/password/email', 'MemberPasswordController@postEmail');

// Password reset routes...
Route::get('/member/password/reset/{token}', 'MemberPasswordController@getReset');
Route::post('/member/password/reset', 'MemberPasswordController@postReset'); 


Route::controllers([  
	'member' => 'MemberController', 
]); 

Route::get('/catalog/newproduct', [
     'middleware' => ['auth.front', 'roles'],
     'uses' => 'HomeController@newproduct',
     'roles' => ['member']
]);  

Route::get('/catalog/product/{slug}', [
     'middleware' => ['auth.front', 'roles'],
     'uses' => 'HomeController@product',
     'roles' => ['member']
]);  

Route::get('/catalog/category/{id}', [
     'middleware' => ['auth.front', 'roles'],
     'uses' => 'HomeController@category',
     'roles' => ['member']
]);  

Route::get('/', 'HomeController@getIndex'); 

Route::get('/locale/{code}', 'HomeController@switchLocale');
Route::get('/contact-us', 'HomeController@getContactUs');

Route::get('/searchresult', [
     'middleware' => ['auth.front', 'roles'],
     'uses' => 'HomeController@searchresult',
     'roles' => ['member']
]);  

Route::get('/inquiry', [
     'middleware' => ['auth.front', 'roles'],
     'uses' => 'HomeController@getInquiry',
     'roles' => ['member']
]);  
 
Route::get('/addtoinquiry', 'HomeController@addtoinquiry');
Route::post('/contact-us/submit', 'HomeController@submitMessage'); 


Route::get('admin', ['middleware' => ['auth', 'roles'],'uses' => 'AdminController@index','roles' => ['admin']]); 

// page routes...
Route::get('admin/manage/page', ['middleware' => ['auth', 'roles'],'uses' => 'PageController@index', 'roles' => ['admin']]);
Route::get('admin/manage/page/create', ['middleware' => ['auth', 'roles'],'uses' => 'PageController@create', 'roles' => ['admin']]); 
Route::get('admin/manage/page/edit/{page_id}', ['middleware' => ['auth', 'roles'],'uses' => 'PageController@edit', 'roles' => ['admin']]); 
Route::get('admin/manage/page/update/{page_id}', ['middleware' => ['auth', 'roles'],'uses' => 'PageController@update', 'roles' => ['admin']]); 
Route::post('manage/page/store', ['middleware' => ['auth', 'roles'],'uses' => 'PageController@store', 'roles' => ['admin']]); 
Route::get('admin/manage/page/setActive/{page_id}', ['middleware' => ['auth', 'roles'],'uses' => 'PageController@setActive', 'roles' => ['admin']]);
Route::get('admin/manage/page/setInactive/{page_id}', ['middleware' => ['auth', 'roles'],'uses' => 'PageController@setInactive', 'roles' => ['admin']]);
Route::get('admin/manage/page/destroy/{page_id}', ['middleware' => ['auth', 'roles'],'uses' => 'PageController@destroy', 'roles' => ['admin']]); 

// Member routes
Route::get('admin/manage/user', 'UserController@index');
Route::get('admin/manage/user/show/{user_id}', 'UserController@show');
Route::get('admin/manage/user/setActive/{user_id}', 'UserController@setActive');
Route::get('admin/manage/user/setInactive/{user_id}', 'UserController@setInactive');
Route::get('admin/manage/user/destroy/{user_id}', 'UserController@destroy');

// File routes
Route::get('admin/manage/file', 'FileController@index');
Route::get('admin/manage/file/show/{file_id}', 'FileController@show');
Route::get('admin/manage/file/setActive/{file_id}', 'FileController@setActive');
Route::get('admin/manage/file/setInactive/{file_id}', 'FileController@setInactive');
Route::get('admin/manage/file/destroy/{file_id}', 'FileController@destroy');

// Catalog routes...
Route::get('admin/manage/catalog', 'CatalogController@index');
Route::post('manage/catalog/massdelete', 'CatalogController@massdelete');
Route::get('admin/manage/catalog/import/{catalog_id}', 'CatalogController@import');
Route::post('manage/catalog/import', 'CatalogController@storeimport');
Route::get('admin/manage/catalog/create', 'CatalogController@create');
Route::get('admin/manage/catalog/edit/{catalog_id}', 'CatalogController@edit');
Route::put('manage/catalog/update/{catalog_id}', 'CatalogController@update');
Route::post('manage/catalog/store', 'CatalogController@store');
Route::get('admin/manage/catalog/setActive/{catalog_id}', 'CatalogController@setActive');
Route::get('admin/manage/catalog/setInactive/{catalog_id}', 'CatalogController@setInactive');
Route::get('admin/manage/catalog/destroy/{catalog_id}', 'CatalogController@destroy');

// Contact us routes
Route::get('admin/manage/message', ['middleware' => ['auth', 'roles'],'uses' => 'MessageController@index', 'roles' => ['admin']]);
Route::get('admin/manage/message/show/{msg_id}', ['middleware' => ['auth', 'roles'],'uses' => 'MessageController@show', 'roles' => ['admin']]);
Route::get('admin/manage/message/setActive/{msg_id}', ['middleware' => ['auth', 'roles'],'uses' => 'MessageController@setActive', 'roles' => ['admin']]);
Route::get('admin/manage/message/setInactive/{msg_id}', ['middleware' => ['auth', 'roles'],'uses' => 'MessageController@setInactive', 'roles' => ['admin']]);
Route::get('admin/manage/message/destroy/{msg_id}', ['middleware' => ['auth', 'roles'],'uses' => 'MessageController@destroy', 'roles' => ['admin']]);

// Category routes...
Route::get('admin/manage/category', ['middleware' => ['auth', 'roles'],'uses' => 'CategoryController@index', 'roles' => ['admin']]);
Route::get('admin/manage/category/create', ['middleware' => ['auth', 'roles'],'uses' => 'CategoryController@create', 'roles' => ['admin']]);
Route::get('admin/manage/category/edit/{category_id}', ['middleware' => ['auth', 'roles'],'uses' => 'CategoryController@edit', 'roles' => ['admin']]);
Route::put('manage/category/update/{category_id}', ['middleware' => ['auth', 'roles'],'uses' => 'CategoryController@update', 'roles' => ['admin']]);
Route::post('manage/category/store', ['middleware' => ['auth', 'roles'],'uses' => 'CategoryController@store', 'roles' => ['admin']]);
Route::get('admin/manage/category/setActive/{category_id}', ['middleware' => ['auth', 'roles'],'uses' => 'CategoryController@setActive', 'roles' => ['admin']]);
Route::get('admin/manage/category/setInactive/{category_id}', ['middleware' => ['auth', 'roles'],'uses' => 'CategoryController@setInactive', 'roles' => ['admin']]);
Route::get('admin/manage/category/destroy/{category_id}', ['middleware' => ['auth', 'roles'],'uses' => 'CategoryController@destroy', 'roles' => ['admin']]);

// Setting routes...
Route::get('admin/manage/setting', ['middleware' => ['auth', 'roles'],'uses' => 'SettingController@edit', 'roles' => ['admin']]);
Route::put('manage/setting/update', ['middleware' => ['auth', 'roles'],'uses' => 'SettingController@update', 'roles' => ['admin']]);

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

/*if no matching router , all goes to here*/
/*Route::any('{all}','HomeController@getStaticPages')->where('all', '.*');*/