<?php

/*
 * This file is part of Laravel Translator.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * This is the locales table seeder class.
 *
 * @author Vincent Klaiber <hello@vinkla.com>
 */
class CreateLocalesTable extends Migration
{
   
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('locales');

        Schema::create('locales', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('language')->nullable(false);
            $table->integer('status')->default(2);
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('locales');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
