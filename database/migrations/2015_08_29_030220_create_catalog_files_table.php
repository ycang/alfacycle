<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::dropIfExists('catalog_files');

		Schema::create('catalog_files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('catalog_id')->unsigned(true);
			$table->foreign('catalog_id')->references('id')->on('catalogs');
			$table->integer('img_id')->unsigned(true);
			$table->foreign('img_id')->references('id')->on('files');
			$table->integer('sort_order');
			$table->integer('status')->default(2);
			$table->boolean('delete')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('catalog_files');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
