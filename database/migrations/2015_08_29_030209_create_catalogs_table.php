<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::dropIfExists('catalogs');

		Schema::create('catalogs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned(true);
			$table->foreign('category_id')->references('id')->on('categories');
			$table->integer('grid_img_id')->unsigned(true)->nullable(true);
			$table->foreign('grid_img_id')->references('id')->on('files');
			$table->string('year');
			$table->string('slug')->unique();
			$table->integer('sort_order');
			$table->integer('status')->default(2);
			$table->boolean('delete')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('catalogs');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
