<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('company');
			$table->string('website');
			$table->string('telephone');
			$table->string('fax');
			$table->string('country');
			$table->string('gender');
			$table->string('address');
			$table->string('email')->unique();
			$table->string('password', 60); 
			$table->string('role')->default('member');
			$table->integer('status')->default(1);
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
