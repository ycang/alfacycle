<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::dropIfExists('catalog_translations');

		Schema::create('catalog_translations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('catalog_id')->unsigned(true);
			$table->foreign('catalog_id')->references('id')->on('catalogs');
			$table->integer('locale_id')->unsigned(true);
			$table->foreign('locale_id')->references('id')->on('locales');
			$table->string('name');
			$table->string('description');
			$table->integer('status')->default(2);
			$table->boolean('delete')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('catalog_translations');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
