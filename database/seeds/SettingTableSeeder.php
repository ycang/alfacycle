<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Setting;

class SettingTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$settings = array(
			array(
				'name'  => 'Company Address',
				'code'  => 'address',
				'type'  => Setting::USER,
				'value' => 'No.4, Lane 4, Xinzhong Street, Taipei, Taiwan',
			),
			array(
				'name'  => 'Company Phone Number',
				'code'  => 'phone',
				'type'  => Setting::USER,
				'value' => '02 - 2762 - 2382',
			),
			array(
				'name'  => 'Company Fax Number',
				'code'  => 'fax',
				'type'  => Setting::USER,
				'value' => '02 - 2746 - 7436',
			),
			array(
				'name'  => 'Company Email',
				'code'  => 'email',
				'type'  => Setting::USER,
				'value' => 'Info@alfacycle.com.tw',
			),
			array(
				'name'  => 'Facebook Link',
				'code'  => 'facebook_link',
				'type'  => Setting::USER,
				'value' => '',
			),
			array(
				'name'  => 'Youtube Link',
				'code'  => 'youtube_link',
				'type'  => Setting::USER,
				'value' => '',
			),
			array(
				'name'  => 'Instagram Link',
				'code'  => 'instagram_link',
				'type'  => Setting::USER,
				'value' => '',
			),
			array(
				'name'  => 'Google Analytics Key',
				'code'  => 'ga_key',
				'type'  => Setting::SITE,
				'value' => '',
			),
			array(
				'name'  => 'Google Maps Latitude',
				'code'  => 'gmaps_lat',
				'type'  => Setting::SITE,
				'value' => '25.057919',
			),
			array(
				'name'  => 'Google Maps Longitude',
				'code'  => 'gmaps_lng',
				'type'  => Setting::SITE,
				'value' => '121.560663',
			),
			array(
				'name'  => 'Site Title',
				'code'  => 'site_title',
				'type'  => Setting::SITE,
				'value' => 'Alfa Cycle Supply | ',
			),
			array(
				'name'  => 'Meta Keyword',
				'code'  => 'meta_keyword',
				'type'  => Setting::SITE,
				'value' => '',
			),
			array(
				'name'  => 'Meta Description',
				'code'  => 'meta_desc',
				'type'  => Setting::SITE,
				'value' => 'Alfa Cycle Supply',
			),
			array(
				'name'  => 'Meta Image',
				'code'  => 'meta_img_id',
				'type'  => Setting::SITE,
				'value' => '',
			),
			array(
				'name'  => 'Homepage Background Image',
				'code'  => 'homepage_bg_img',
				'type'  => Setting::SITE,
				'value' => '',
			),
		);

		foreach ($settings as $setting)
		{
			Setting::create($setting);
		}
	}

}
