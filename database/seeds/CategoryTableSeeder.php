	<?php

use Illuminate\Database\Seeder;
use App\Models\Locale;
use App\Models\Files;
use App\Models\Category;
use App\Models\CategoryTranslation;

class CategoryTableSeeder extends Seeder 
{
	public function run()
	{
		// First file to be seeded 
		$categories = array(
			array('translation' => array('en' => array(
						'name' => 'ACCESSORIES',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			array('translation' => array( 'en' => array(
						'name' => 'GOGGLES',),),'sort_order' => '0',
				'parent_id' => '1',
			), 
			array('translation' => array('en' => array(
						'name' => 'VISORS',),),'sort_order' => '0',
				'parent_id' => '1',
			),  
			array('translation' => array('en' => array(
						'name' => 'CARGO NETS',),),'sort_order' => '0',
				'parent_id' => '1',
			),  
			array('translation' => array('en' => array(
						'name' => 'LUGGAGE ELASTICS',),),'sort_order' => '0',
				'parent_id' => '1',
			),  
			array('translation' => array('en' => array(
						'name' => 'M/C COVERS',),),'sort_order' => '0',
				'parent_id' => '1',
			),  
			/*7*/
			array('translation' => array('en' => array(
						'name' => 'LUGGAGE',),),'sort_order' => '0',
				'parent_id' => '1',
			),   
			array('translation' => array('en' => array(
						'name' => 'TOP BOXES',),),'sort_order' => '0',
				'parent_id' => '7',
			),  
			array('translation' => array('en' => array(
						'name' => 'TOOL BAGS',),),'sort_order' => '0',
				'parent_id' => '7',
			),  
			array('translation' => array('en' => array(
						'name' => 'DRINKS SYSTEMS',),),'sort_order' => '0',
				'parent_id' => '7',
			),  
			array('translation' => array('en' => array(
						'name' => 'CARRIER BAGS',),),'sort_order' => '0',
				'parent_id' => '7',
			),  
			array('translation' => array('en' => array(
						'name' => 'HELMET BAGS',),),'sort_order' => '0',
				'parent_id' => '7',
			),  
			array('translation' => array('en' => array(
						'name' => 'SADDLE BAGS',),),'sort_order' => '0',
				'parent_id' => '7',
			),  
			array('translation' => array('en' => array(
						'name' => 'TANK BAGS',),),'sort_order' => '0',
				'parent_id' => '7',
			),  
			array('translation' => array('en' => array(
						'name' => 'TOOL BAGS',),),'sort_order' => '0',
				'parent_id' => '7',
			),  
			/*16*/ 
			array('translation' => array('en' => array(
						'name' => 'TANK PADS',),),'sort_order' => '0',
				'parent_id' => '1',
			), 
			array('translation' => array('en' => array(
						'name' => 'TAX DISC HOLDERS',),),'sort_order' => '0',
				'parent_id' => '1',
			), 
			array('translation' => array('en' => array(
						'name' => 'PADDOCK STANDS',),),'sort_order' => '0',
				'parent_id' => '1',
			), 
			array('translation' => array('en' => array(
						'name' => 'STANDS',),),'sort_order' => '0',
				'parent_id' => '1',
			), 
			array('translation' => array('en' => array(
						'name' => 'LOCKS & SECURITY',),),'sort_order' => '0',
				'parent_id' => '1',
			),
			array('translation' => array('en' => array(
						'name' => 'ALARMS',),),'sort_order' => '0',
				'parent_id' => '20',
			), 
			array('translation' => array('en' => array(
						'name' => 'CHAIN & FRAME LOCKS',),),'sort_order' => '0',
				'parent_id' => '20',
			), 
			array('translation' => array('en' => array(
						'name' => 'DISC LOCKS',),),'sort_order' => '0',
				'parent_id' => '20',
			), 
			array('translation' => array('en' => array(
						'name' => 'PADLOCKS',),),'sort_order' => '0',
				'parent_id' => '20',
			), 
			/*25*/
			array('translation' => array('en' => array(
						'name' => 'MISCELLANEOUS',),),'sort_order' => '0',
				'parent_id' => '1',
			),  	
				array('translation' => array('en' => array(
							'name' => 'FACE MASKS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'NECK WARMERS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'GLOVES',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'OVERBOOTS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'HAND MUFFS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'HARNESS & ASSISTS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'HOSE CLIPS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'HOUR METER',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'INTERCOM',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'KNEE PADS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'PILLION TO RIDER HAND GRIP SYSTEM',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'PLASTIC CONTAINERS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 
				array('translation' => array('en' => array(
							'name' => 'REFLECTORS',),),'sort_order' => '0',
					'parent_id' => '25',
				), 

			array('translation' => array('en' => array(
						'name' => 'BRAKES',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
				array('translation' => array('en' => array(
							'name' => 'BRAKES PADS',),), 'sort_order' => '0',
					'parent_id' => '39',
				), 
					array('translation' => array('en' => array(
								'name' => 'HONDA',),), 'sort_order' => '0',
						'parent_id' => '40', 
					), 
					array('translation' => array('en' => array(
								'name' => 'YAMAHA',),), 'sort_order' => '0',
						'parent_id' => '40', 
					), 
					array('translation' => array('en' => array(
								'name' => 'SUZUKI',),), 'sort_order' => '0',
						'parent_id' => '40', 
					), 
					array('translation' => array('en' => array(
								'name' => 'KAWASAKI',),), 'sort_order' => '0',
						'parent_id' => '40', 
					), 
					array('translation' => array('en' => array(
								'name' => 'OTHERS',),), 'sort_order' => '0',
						'parent_id' => '40', 
					),  
				array('translation' => array('en' => array(
							'name' => 'BRAKE SHOES',),), 'sort_order' => '0',
					'parent_id' => '39',
				), 
					array('translation' => array('en' => array(
								'name' => 'HONDA',),), 'sort_order' => '0',
						'parent_id' => '46', 
					), 
					array('translation' => array('en' => array(
								'name' => 'YAMAHA',),), 'sort_order' => '0',
						'parent_id' => '46', 
					), 
					array('translation' => array('en' => array(
								'name' => 'SUZUKI',),), 'sort_order' => '0',
						'parent_id' => '46', 
					), 
					array('translation' => array('en' => array(
								'name' => 'KAWASAKI',),), 'sort_order' => '0',
						'parent_id' => '46', 
					), 
					array('translation' => array('en' => array(
								'name' => 'OTHERS',),), 'sort_order' => '0',
						'parent_id' => '46', 
					), 

				array('translation' => array('en' => array(
							'name' => 'BRAKE CALIPERS',),), 'sort_order' => '0',
					'parent_id' => '39',
				), 
				array('translation' => array('en' => array(
							'name' => 'MASTER CYLINDERS',),), 'sort_order' => '0',
					'parent_id' => '39',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRAKE CALIPER REPAIR KIT',),), 'sort_order' => '0',
					'parent_id' => '39',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRAKE RODS & ACCESSORIES',),), 'sort_order' => '0',
					'parent_id' => '39',
				), 
				array('translation' => array('en' => array(
							'name' => 'MISCELLANEOUS',),), 'sort_order' => '0',
					'parent_id' => '39',
				),
					array('translation' => array('en' => array(
								'name' => 'BRAKE FITTINGS',),), 'sort_order' => '0',
						'parent_id' => '56',
					),  
					array('translation' => array('en' => array(
								'name' => 'BRAKE HOSE & TUBING',),), 'sort_order' => '0',
						'parent_id' => '56',
					),  
					array('translation' => array('en' => array(
								'name' => 'BRAKE PAD PINS',),), 'sort_order' => '0',
						'parent_id' => '56',
					),  
					array('translation' => array('en' => array(
								'name' => 'CALIPER NIPPLES',),), 'sort_order' => '0',
						'parent_id' => '56',
					),  
					array('translation' => array('en' => array(
								'name' => 'CALIPER SHAFT BOOTS',),), 'sort_order' => '0',
						'parent_id' => '56',
					),   
					array('translation' => array('en' => array(
								'name' => 'MASTER CYLINDER CAPS',),), 'sort_order' => '0',
						'parent_id' => '56',
					),   
					array('translation' => array('en' => array(
								'name' => 'PIPE CLAMPS',),), 'sort_order' => '0',
						'parent_id' => '56',
					),   
					array('translation' => array('en' => array(
								'name' => 'PISTON & SEAL KITS',),), 'sort_order' => '0',
						'parent_id' => '56',
					),   
					array('translation' => array('en' => array(
								'name' => 'BRAKE SWITCHES',),), 'sort_order' => '0',
						'parent_id' => '56',
					),    
			array('translation' => array('en' => array(
						'name' => 'CABLES',),), 'sort_order' => '0',
				'parent_id' => '537',
			),  
				array('translation' => array('en' => array(
							'name' => 'THROTTLE',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'SPEEDO',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'TACHO',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'FRONT BRAKE',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'REAR BRAKE',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'CHOKE',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'UNIVERSAL',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'CABLE NIPPLES',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'FERRULES & FITTINGS',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'INNER CABLE',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'OUTER CABLE',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'CABLE COVER',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'SPEEDO DRIVES',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
				array('translation' => array('en' => array(
							'name' => 'CABLE ADJUSTERS',),), 'sort_order' => '0',
					'parent_id' => '66',
				), 
			/*82*/
			array('translation' => array('en' => array(
						'name' => 'CARBURETORS',),), 'sort_order' => '0',
				'parent_id' => '537',
			),  
				array('translation' => array('en' => array(
							'name' => 'FLAT SLIDE CARBURETORS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
				array('translation' => array('en' => array(
							'name' => 'ROUND SLIDE CARBURETORS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
				array('translation' => array('en' => array(
							'name' => 'PWK CARBS AND PARTS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
				array('translation' => array('en' => array(
							'name' => 'DELLORTO PERFORMANCE CARBS & PARTS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
				array('translation' => array('en' => array(
							'name' => 'CARBURETORS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
					array('translation' => array('en' => array(
								'name' => 'FLA',),), 'sort_order' => '0',
						'parent_id' => '87',
					),  
				array('translation' => array('en' => array(
							'name' => 'CARB REPAIR KITS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
				array('translation' => array('en' => array(
							'name' => 'CARB PARTS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
					array('translation' => array('en' => array(
								'name' => 'GASKETS & SEALS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'CHOKES & PLUNGERS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'FLOAT BOWLS & PLUGS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'FLOAT VALVES',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'FLOATS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'SLIDES',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'SLIDE NEEDLES',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'NEEDLE JETS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'MIXTURE SCREWS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'IDLE ADJUSTER',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'PILOT JETS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'AIR & FUEL SCREWS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'THROTTLE SPRINGS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'MISC HARDWARE',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'CARB BODY TOPS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
					array('translation' => array('en' => array(
								'name' => 'THROTTLE BUTTERFLY PARTS',),), 'sort_order' => '0',
						'parent_id' => '90',
					), 
				array('translation' => array('en' => array(
							'name' => 'CARB DAPHRAGMS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
				array('translation' => array('en' => array(
							'name' => 'JETTING & TUNING',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS - DELI ORTO SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS - GURTNER SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS - KEIHIN - 0.80 PITCH KAWASAKI HONDA SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS - KEIHIN - LONG HEX TYPE SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS - MIKUNI - LARGE ROUND TYPE SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS - MIKUNI - SHORT HEX TYPE SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS - MIKUNI - SMALL ROUND TYPE SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'MAIN JETS - OTHER SECTION',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'PILOT JETS',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'SLIDES',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'PUMP NOZZLE',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'STARTER JET',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'AIR JET',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'NEEDLE JETS',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'NEEDLE VALVES',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
					array('translation' => array('en' => array(
								'name' => 'NEEDLES',),), 'sort_order' => '0',
						'parent_id' => '108',
					), 
				array('translation' => array('en' => array(
							'name' => 'FLANGE ADAPTERS',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
					array('translation' => array('en' => array(
								'name' => 'INTAKE MANIFLODS',),), 'sort_order' => '0',
						'parent_id' => '126',
					), 
					array('translation' => array('en' => array(
								'name' => 'CARB HODLERS',),), 'sort_order' => '0',
						'parent_id' => '126',
					), 
					array('translation' => array('en' => array(
								'name' => 'CARB TO HEAD RUBBERS',),), 'sort_order' => '0',
						'parent_id' => '126',
					), 
					array('translation' => array('en' => array(
								'name' => 'AIR CUT OFF VALVE SETS',),), 'sort_order' => '0',
						'parent_id' => '126',
					), 
					array('translation' => array('en' => array(
								'name' => 'FUEL LINE',),), 'sort_order' => '0',
						'parent_id' => '126',
					), 
					array('translation' => array('en' => array(
								'name' => 'ELECTRONIC CHOKES',),), 'sort_order' => '0',
						'parent_id' => '126',
					), 
					/*133*/
				array('translation' => array('en' => array(
							'name' => 'ACCESSORIES',),), 'sort_order' => '0',
					'parent_id' => '82',
				), 
					array('translation' => array('en' => array(
								'name' => 'BREATHER VENT PARTS',),), 'sort_order' => '0',
						'parent_id' => '133',
					), 
					array('translation' => array('en' => array(
								'name' => 'OIL & OVERFLOW PIPE',),), 'sort_order' => '0',
						'parent_id' => '133',
					), 
					array('translation' => array('en' => array(
								'name' => 'PATROL PIPE ',),), 'sort_order' => '0',
						'parent_id' => '133',
					), 
				/*137*/
			array('translation' => array('en' => array(
						'name' => 'CHASSIS & BODY',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
				array('translation' => array('en' => array(
							'name' => 'BEARINGS',),), 'sort_order' => '0',
					'parent_id' => '137',
				),  
				array('translation' => array('en' => array(
							'name' => 'FAIRING & WINDSHIELDS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
					array('translation' => array('en' => array(
								'name' => 'FAIRINGS',),), 'sort_order' => '0',
						'parent_id' => '139',
					),   
					array('translation' => array('en' => array(
								'name' => 'FAIRING WITH HEADLIGHT',),), 'sort_order' => '0',
						'parent_id' => '139',
					),   
					array('translation' => array('en' => array(
								'name' => 'FAIRINGS BRACKETS',),), 'sort_order' => '0',
						'parent_id' => '139',
					),   
				array('translation' => array('en' => array(
							'name' => 'BADGES',),), 'sort_order' => '0',
					'parent_id' => '137',
				),   
					array('translation' => array('en' => array(
								'name' => 'EMBLEMS & BADGES',),), 'sort_order' => '0',
						'parent_id' => '143',
					),  
				array('translation' => array('en' => array(
							'name' => 'SEAT',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
					array('translation' => array('en' => array(
								'name' => 'SEAT',),), 'sort_order' => '0',
						'parent_id' => '145',
					), 
					array('translation' => array('en' => array(
								'name' => 'SEAT BRACKETS',),), 'sort_order' => '0',
						'parent_id' => '145',
					), 
					array('translation' => array('en' => array(
								'name' => 'SEAT RAILS & BRACKETS',),), 'sort_order' => '0',
						'parent_id' => '145',
					), 
					array('translation' => array('en' => array(
								'name' => 'SEAT SPRINGS',),), 'sort_order' => '0',
						'parent_id' => '145',
					), 
					array('translation' => array('en' => array(
								'name' => 'SEAT COVERS',),), 'sort_order' => '0',
						'parent_id' => '145',
					), 
					array('translation' => array('en' => array(
								'name' => 'SEAT PANELS',),), 'sort_order' => '0',
						'parent_id' => '145',
					), 
				array('translation' => array('en' => array(
							'name' => 'PLATE HOLDERS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
				array('translation' => array('en' => array(
							'name' => 'FENDERS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
					array('translation' => array('en' => array(
								'name' => 'FENDERS',),), 'sort_order' => '0',
						'parent_id' => '153',
					), 
					array('translation' => array('en' => array(
								'name' => 'MUDGUARDS, FRONT',),), 'sort_order' => '0',
						'parent_id' => '153',
					), 
					array('translation' => array('en' => array(
								'name' => 'MUDGUARDS, REAR',),), 'sort_order' => '0',
						'parent_id' => '153',
					), 
					array('translation' => array('en' => array(
								'name' => 'MUDFLAPS',),), 'sort_order' => '0',
						'parent_id' => '153',
					), 
				array('translation' => array('en' => array(
							'name' => 'CARRIER RACKS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
				array('translation' => array('en' => array(
							'name' => 'FRAME SLIDERS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
				array('translation' => array('en' => array(
							'name' => 'SIDED COVERS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
				array('translation' => array('en' => array(
							'name' => 'ENGINE GUARDS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
				array('translation' => array('en' => array(
							'name' => 'CHAIN GUIDES',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
				array('translation' => array('en' => array(
							'name' => 'LEGSHIELDS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
				array('translation' => array('en' => array(
							'name' => 'RUBBERS & GROMMETS',),), 'sort_order' => '0',
					'parent_id' => '137',
				), 
				/*165*/ 
			array('translation' => array('en' => array(
						'name' => 'FRAME',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
				array('translation' => array('en' => array(
							'name' => 'SIDE STAND',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'MAIN STAND',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'FOOTRESTS',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
					array('translation' => array('en' => array(
								'name' => 'RACE PEGS',),), 'sort_order' => '0',
						'parent_id' => '168',
					), 
					array('translation' => array('en' => array(
								'name' => 'OE STYLE',),), 'sort_order' => '0',
						'parent_id' => '168',
					), 
					array('translation' => array('en' => array(
								'name' => 'UNIVERSAL',),), 'sort_order' => '0',
						'parent_id' => '168',
					), 
					array('translation' => array('en' => array(
								'name' => 'FLOORBOARDS',),), 'sort_order' => '0',
						'parent_id' => '168',
					), 
				array('translation' => array('en' => array(
							'name' => 'FOOTPEG MOUNTS',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRAKE PEDALS',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'FOOT BRAKES',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRAKE SNAKE',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'RUBBERS',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'LIFT HANDLES',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
			array('translation' => array('en' => array(
						'name' => 'CHAIN & SPROCKETS',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
				array('translation' => array('en' => array(
							'name' => 'CHAIN',),), 'sort_order' => '0',
					'parent_id' => '179',
				), 
					array('translation' => array('en' => array(
								'name' => 'BULK CHAIN',),), 'sort_order' => '0',
						'parent_id' => '180',
					), 
					array('translation' => array('en' => array(
								'name' => 'CAM CHAINS',),), 'sort_order' => '0',
						'parent_id' => '180',
					), 
					array('translation' => array('en' => array(
								'name' => 'DRIVE CHAINS',),), 'sort_order' => '0',
						'parent_id' => '180',
					), 
					array('translation' => array('en' => array(
								'name' => 'LINKS',),), 'sort_order' => '0',
						'parent_id' => '180',
					), 
				array('translation' => array('en' => array(
							'name' => 'CHAIN TOOLS',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'CHAIN ADJUSTER',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				array('translation' => array('en' => array(
							'name' => 'DRIVE BELTS',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
				/*188*/
				array('translation' => array('en' => array(
							'name' => 'SPROCKETS',),), 'sort_order' => '0',
					'parent_id' => '165',
				), 
					array('translation' => array('en' => array(
								'name' => 'FRONT SPROCKETS',),), 'sort_order' => '0',
						'parent_id' => '188',
					), 
					array('translation' => array('en' => array(
								'name' => 'REAR SPROCKETS',),), 'sort_order' => '0',
						'parent_id' => '188',
					), 
					array('translation' => array('en' => array(
								'name' => 'SPROCKET BOLT & TAB SETS',),), 'sort_order' => '0',
						'parent_id' => '188',
					), 
					array('translation' => array('en' => array(
								'name' => 'SPROCKET CIRCLIPS & RETAINERS',),), 'sort_order' => '0',
						'parent_id' => '188',
					), 
					array('translation' => array('en' => array(
								'name' => 'SPROCKET DRIVE RUBBERS',),), 'sort_order' => '0',
						'parent_id' => '188',
					), 
					array('translation' => array('en' => array(
								'name' => 'DISCS',),), 'sort_order' => '0',
						'parent_id' => '188',
					),  
			/*195*/
			array('translation' => array('en' => array(
						'name' => 'ELECTRICAL',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*196*/
			array('translation' => array('en' => array(
						'name' => 'ENGINE',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*197*/
			array('translation' => array('en' => array(
						'name' => 'EXHAUST',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*198*/
			array('translation' => array('en' => array(
						'name' => 'FUEL & AIR',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*199*/
			array('translation' => array('en' => array(
						'name' => 'GASKETS & SEALS',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*200*/
			array('translation' => array('en' => array(
						'name' => 'HANDLEBAR & CONTROLS',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*201*/
			array('translation' => array('en' => array(
						'name' => 'LEVERS',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*202*/
			array('translation' => array('en' => array(
						'name' => 'OIL FILTERS',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*203*/
			array('translation' => array('en' => array(
						'name' => 'MIRRORS',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*204*/
			array('translation' => array('en' => array(
						'name' => 'LIGHTING',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*205*/
			array('translation' => array('en' => array(
						'name' => 'SUSPENSION',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*206*/
			array('translation' => array('en' => array(
						'name' => 'STORAGE/GUARDS/STANDS',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*207*/
			array('translation' => array('en' => array(
						'name' => 'INSTRUMENT',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*208*/
			array('translation' => array('en' => array(
						'name' => 'TIRES & WHEELS',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*209*/
			array('translation' => array('en' => array(
						'name' => 'TOOLS/WORKSHOP',),), 'sort_order' => '0',
				'parent_id' => '537',
			), 
			/*210*/
			array('translation' => array('en' => array(
						'name' => 'FASTENERS & FIXING',),), 'sort_order' => '0',
				'parent_id' => '537',
			),  
				array('translation' => array('en' => array(
							'name' => 'FUSE BOXES',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'FUSES & ACCESSORIES',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'BATTERIES',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'BULBS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'CDI UNIT',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'POINTS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'CONDESERS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'COILS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'TERMINALS & COUPLERS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'CABLE & CONNECTORS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'INDICATOR RELAYS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'IGNITION SWITCHES',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'HORNS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'PLUG CAPS & WIRE',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'STARTER MOTORS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'HANDLEBAR SWITCHES',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'RECTIFIERS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'REGULATORS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'STARTER SOLENOIDS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'WIRE HARNESSES',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'ROTORS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRUSHES',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'STATORS',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 
				array('translation' => array('en' => array(
							'name' => 'SWITCHES',),), 'sort_order' => '0',
					'parent_id' => '195',
				), 

				array('translation' => array('en' => array(
							'name' => 'CRANKSHAFTS',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 
				array('translation' => array('en' => array(
							'name' => 'CONNECTING RODS',),), 'sort_order' => '0',
					'parent_id' => '196',
				),
				array('translation' => array('en' => array(
							'name' => 'CYLINDER & HEADS',),), 'sort_order' => '0',
					'parent_id' => '196',
				),  
				array('translation' => array('en' => array(
							'name' => 'ENGINE COVERS',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 
				array('translation' => array('en' => array(
							'name' => 'CAMSHAFTS',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 
				array('translation' => array('en' => array(
							'name' => 'ENGINE VALVES',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 
				array('translation' => array('en' => array(
							'name' => 'PISTONS',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH SPRING BOLTS',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH ROLLER WEIGHTS',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 
				array('translation' => array('en' => array(
							'name' => 'MISCELLANEOUS',),), 'sort_order' => '0',
					'parent_id' => '196',
				), 

				array('translation' => array('en' => array(
							'name' => 'HEAD PIPES',),), 'sort_order' => '0',
					'parent_id' => '197',
				), 
				array('translation' => array('en' => array(
							'name' => 'MUFFLERS',),), 'sort_order' => '0',
					'parent_id' => '197',
				), 
				array('translation' => array('en' => array(
							'name' => 'BAFFLES',),), 'sort_order' => '0',
					'parent_id' => '197',
				),
				array('translation' => array('en' => array(
							'name' => 'EXHAUST GASKETS',),), 'sort_order' => '0',
					'parent_id' => '197',
				),  
				array('translation' => array('en' => array(
							'name' => 'HARDWARE',),), 'sort_order' => '0',
					'parent_id' => '197',
				), 

				array('translation' => array('en' => array(
							'name' => 'AIR FILTERS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'POWER FILTERS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'FUEL FILTER',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'FUEL LINE',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'FUEL PUMPS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'GAS TANKS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'GAS CAPS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'PETCOCKS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'PETCOCK REPAIR KIT',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'GAS CAP VENT & HOSE',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'RADIATORS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'RADIATOR CAPS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'WATER PUMPS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'WATER PUMPS REPAIR KITS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'THERMOSTATS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 
				array('translation' => array('en' => array(
							'name' => 'MISCELLANEOUS',),), 'sort_order' => '0',
					'parent_id' => '198',
				), 

				array('translation' => array('en' => array(
							'name' => 'COMPLETE SETS',),), 'sort_order' => '0',
					'parent_id' => '199',
				), 
				array('translation' => array('en' => array(
							'name' => 'TOP END GASKETS',),), 'sort_order' => '0',
					'parent_id' => '199',
				), 
				array('translation' => array('en' => array(
							'name' => 'BUTTOM END GASKETS',),), 'sort_order' => '0',
					'parent_id' => '199',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH COVER GASKETS',),), 'sort_order' => '0',
					'parent_id' => '199',
				), 
				array('translation' => array('en' => array(
							'name' => 'CYLINDER BASE GASKETS',),), 'sort_order' => '0',
					'parent_id' => '199',
				), 
				array('translation' => array('en' => array(
							'name' => 'CYLINDER HEAD GASKETS',),), 'sort_order' => '0',
					'parent_id' => '199',
				), 
				array('translation' => array('en' => array(
							'name' => 'ENGINE OIL SEALS',),), 'sort_order' => '0',
					'parent_id' => '199',
				),  
				array('translation' => array('en' => array(
							'name' => 'O-RINGS',),), 'sort_order' => '0',
					'parent_id' => '199',
				), 

				array('translation' => array('en' => array(
							'name' => 'HANDLEBARS',),), 'sort_order' => '0',
					'parent_id' => '200',
				),
				array('translation' => array('en' => array(
							'name' => 'BRUSH GUARDS',),), 'sort_order' => '0',
					'parent_id' => '200',
				),  
				array('translation' => array('en' => array(
							'name' => 'BAR ENDS',),), 'sort_order' => '0',
					'parent_id' => '200',
				), 
				array('translation' => array('en' => array(
							'name' => 'HAND GRIPS',),), 'sort_order' => '0',
					'parent_id' => '200',
				), 
				array('translation' => array('en' => array(
							'name' => 'MASTER CYLINDERS',),), 'sort_order' => '0',
					'parent_id' => '200',
				), 
				array('translation' => array('en' => array(
							'name' => 'MASTER CYLINDER REPAIR KITS',),), 'sort_order' => '0',
					'parent_id' => '200',
				), 
				array('translation' => array('en' => array(
							'name' => 'RISERS',),), 'sort_order' => '0',
					'parent_id' => '200',
				), 
				array('translation' => array('en' => array(
							'name' => 'THROTTLE',),), 'sort_order' => '0',
					'parent_id' => '200',
				), 

				array('translation' => array('en' => array(
							'name' => 'BRAKE - HONDA',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRAKE - YAMAHA',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRAKE - SUZUKI',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRAKE - KAWASAKI',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'BRAKE - OTHERS',),), 'sort_order' => '0',
					'parent_id' => '201',
				),
				array('translation' => array('en' => array(
							'name' => 'CLUTCH - HONDA',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH - YAMAHA',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH - SUZUKI',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH - KAWASAKI',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'CLUTCH - OTHERS',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'FORGED - HONDA',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'FORGED - YAMAHA',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'FORGED - SUZUKI',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'FORGED - KAWASAKI',),), 'sort_order' => '0',
					'parent_id' => '201',
				), 
				array('translation' => array('en' => array(
							'name' => 'FORGED - OTHERS',),), 'sort_order' => '0',
					'parent_id' => '201',
				),
				array('translation' => array('en' => array(
							'name' => 'LEVER ASSEMBLIES',),), 'sort_order' => '0',
					'parent_id' => '201',
				),
				array('translation' => array('en' => array(
							'name' => 'LEVER BRACKETS',),), 'sort_order' => '0',
					'parent_id' => '201',
				),
				array('translation' => array('en' => array(
							'name' => 'LEVER SETS',),), 'sort_order' => '0',
					'parent_id' => '201',
				),
				array('translation' => array('en' => array(
							'name' => 'LEVER ACCESSORIES',),), 'sort_order' => '0',
					'parent_id' => '201',
				),
				array('translation' => array('en' => array(
							'name' => 'KICKSTART LEVERS',),), 'sort_order' => '0',
					'parent_id' => '201',
				),
				array('translation' => array('en' => array(
							'name' => 'SHIFT LEVERS',),), 'sort_order' => '0',
					'parent_id' => '201',
				),

				array('translation' => array('en' => array(
							'name' => 'HONDA',),), 'sort_order' => '0',
					'parent_id' => '202',
				), 
				array('translation' => array('en' => array(
							'name' => 'YAMAHA',),), 'sort_order' => '0',
					'parent_id' => '202',
				), 
				array('translation' => array('en' => array(
							'name' => 'SUZUKI',),), 'sort_order' => '0',
					'parent_id' => '202',
				), 
				array('translation' => array('en' => array(
							'name' => 'KAWASAKI',),), 'sort_order' => '0',
					'parent_id' => '202',
				), 
				array('translation' => array('en' => array(
							'name' => 'OTHERS',),), 'sort_order' => '0',
					'parent_id' => '202',
				),
				array('translation' => array('en' => array(
							'name' => 'O-RING SETS',),), 'sort_order' => '0',
					'parent_id' => '202',
				),
				array('translation' => array('en' => array(
							'name' => 'OIL FILTER BOLTS',),), 'sort_order' => '0',
					'parent_id' => '202',
				),

				array('translation' => array('en' => array(
							'name' => 'HONDA',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'YAMAHA',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'SUZUKI',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'KAWASAKI',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'EUROPEAN',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'UNIVERSAL',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'BAR END',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'CLAMP ON',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'CUSTOM',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'BRACKETS',),), 'sort_order' => '0',
					'parent_id' => '203',
				),
				array('translation' => array('en' => array(
							'name' => 'ADAPTORS & EXTENSIONS',),), 'sort_order' => '0',
					'parent_id' => '203',
				),

				array('translation' => array('en' => array(
							'name' => 'HEADLIGHT',),), 'sort_order' => '0',
					'parent_id' => '204',
				),
				array('translation' => array('en' => array(
							'name' => 'TAILLIGHT & LENSES',),), 'sort_order' => '0',
					'parent_id' => '204',
				),
				array('translation' => array('en' => array(
							'name' => 'SIGNALS & DECO LIGHTS',),), 'sort_order' => '0',
					'parent_id' => '204',
				),
				array('translation' => array('en' => array(
							'name' => 'MARKERS & SIDE LIGHTS',),), 'sort_order' => '0',
					'parent_id' => '204',
				),
				array('translation' => array('en' => array(
							'name' => 'NUMBER PLATE LIGHTS',),), 'sort_order' => '0',
					'parent_id' => '204',
				),
				array('translation' => array('en' => array(
							'name' => 'BULBS',),), 'sort_order' => '0',
					'parent_id' => '204',
				),

				array('translation' => array('en' => array(
							'name' => 'SHOCKS',),), 'sort_order' => '0',
					'parent_id' => '205',
				),
				array('translation' => array('en' => array(
							'name' => 'FORK TUBES',),), 'sort_order' => '0',
					'parent_id' => '205',
				),
				array('translation' => array('en' => array(
							'name' => 'FORK SEALS',),), 'sort_order' => '0',
					'parent_id' => '205',
				),
				array('translation' => array('en' => array(
							'name' => 'FORK GAITERS',),), 'sort_order' => '0',
					'parent_id' => '205',
				),
				array('translation' => array('en' => array(
							'name' => 'FORK SPRINGS',),), 'sort_order' => '0',
					'parent_id' => '205',
				),
				array('translation' => array('en' => array(
							'name' => 'MISCELLANEOUS',),), 'sort_order' => '0',
					'parent_id' => '205',
				),

				array('translation' => array('en' => array(
							'name' => 'BIKE COVERS',),), 'sort_order' => '0',
					'parent_id' => '206',
				),
				array('translation' => array('en' => array(
							'name' => 'MUD GUARDS',),), 'sort_order' => '0',
					'parent_id' => '206',
				),
				array('translation' => array('en' => array(
							'name' => 'PARTS BOXES',),), 'sort_order' => '0',
					'parent_id' => '206',
				),
				array('translation' => array('en' => array(
							'name' => 'RACE STAND KNOBS',),), 'sort_order' => '0',
					'parent_id' => '206',
				),
				array('translation' => array('en' => array(
							'name' => 'STANDS',),), 'sort_order' => '0',
					'parent_id' => '206',
				),

				array('translation' => array('en' => array(
							'name' => 'GAUGES',),), 'sort_order' => '0',
					'parent_id' => '207',
				),
				array('translation' => array('en' => array(
							'name' => 'GAUGE COVERS',),), 'sort_order' => '0',
					'parent_id' => '207',
				),
				array('translation' => array('en' => array(
							'name' => 'INSTRUMENT BRACKETS',),), 'sort_order' => '0',
					'parent_id' => '207',
				),


				array('translation' => array('en' => array(
							'name' => 'TIRES',),), 'sort_order' => '0',
					'parent_id' => '208',
				),
				array('translation' => array('en' => array(
							'name' => 'TUBES',),), 'sort_order' => '0',
					'parent_id' => '208',
				),
				array('translation' => array('en' => array(
							'name' => 'RIMS',),), 'sort_order' => '0',
					'parent_id' => '208',
				),
				array('translation' => array('en' => array(
							'name' => 'SPOKES',),), 'sort_order' => '0',
					'parent_id' => '208',
				),
				array('translation' => array('en' => array(
							'name' => 'HUBS',),), 'sort_order' => '0',
					'parent_id' => '208',
				),
				array('translation' => array('en' => array(
							'name' => 'RIM TAPE',),), 'sort_order' => '0',
					'parent_id' => '208',
				),
				array('translation' => array('en' => array(
							'name' => 'RIM LOCK',),), 'sort_order' => '0',
					'parent_id' => '208',
				),
				array('translation' => array('en' => array(
							'name' => 'MISCELLANEOUS',),), 'sort_order' => '0',
					'parent_id' => '208',
				),

				array('translation' => array('en' => array(
							'name' => 'ALLEN KEYS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'BATTERY CHARGERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'CABLE TIES',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'CABLE OILERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'CABLE TOOLS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'CHAIN ASSEMBLY TOOLS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'CIRCLIP PLIERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'CLUTCH TOOLS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'ENGINE & BEARING TOOLS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'FEELER GAUGES',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'FILTER WRENCHES',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'FLYWHEEL PULLERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'GENERATOR EXTRACTORS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'LIFTS & RAMPS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'LOCKING WIRE & PLIERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'MEASURING ACCESSORIES',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'NEEDLE FLIES',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'OIL FILTER WRENCH',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'OIL BRAIN & DRIP TRAYS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'PLUG & SPOKE SPANNERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'T0BAR KIT',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'T-BAR SOCKET',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'T-STEM NUT TOOLS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'SUSPENSION & STEERING TOOLS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'SWINGARM TOOLS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'TYRE LEVERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'TYRE BREAKERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'TYRE CHANGERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'VALVE SPRING COMPRESSION KIT',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'VACUUM GAUGES',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'WINCHES',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'SAFETY WIRE PLIERS',),), 'sort_order' => '0',
					'parent_id' => '209',
				),
				array('translation' => array('en' => array(
							'name' => 'SAFETY WIRE',),), 'sort_order' => '0',
					'parent_id' => '209',
				),

				array('translation' => array('en' => array(
							'name' => 'FAIRING SCREWS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'NUTS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'HEX BOLTS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'ALLEN SCREWS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'PAN HEAD SCREWS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'SELF TAPPING SCREWS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'EXHAUST STUDS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'WASHERS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'SPLIT PINS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'CABLE TIES',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'R CLIP',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'E-CLIPS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'SPROCKET BOLTS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'NUMBER PLATE BOLTS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'DISC BOLTS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'HOSE CLAMP',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'ENGINE DRAIN PLUGS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
				array('translation' => array('en' => array(
							'name' => 'RUBBERS',),), 'sort_order' => '0',
					'parent_id' => '210',
				),
					array('translation' => array('en' => array(
								'name' => 'BATTERY ACCESSORIES',),), 'sort_order' => '0',
						'parent_id' => '213',
					),
					array('translation' => array('en' => array(
								'name' => 'BATTERY CABLES',),), 'sort_order' => '0',
						'parent_id' => '213',
					),
					array('translation' => array('en' => array(
								'name' => 'BATTERY CHARGERS',),), 'sort_order' => '0',
						'parent_id' => '213',
					),

					array('translation' => array('en' => array(
								'name' => 'BULBS - HEADLIGHT',),), 'sort_order' => '0',
						'parent_id' => '214',
					),
					array('translation' => array('en' => array(
								'name' => 'BULBS - INDICATOR & SIDE LIGHT',),), 'sort_order' => '0',
						'parent_id' => '214',
					),
					array('translation' => array('en' => array(
								'name' => 'BULBS - Instrument ',),), 'sort_order' => '0',
						'parent_id' => '214',
					),
					array('translation' => array('en' => array(
								'name' => 'BULBS - STOP & TAIL',),), 'sort_order' => '0',
						'parent_id' => '214',
					),

					array('translation' => array('en' => array(
								'name' => 'NEUTRAL SWITCH',),), 'sort_order' => '0',
						'parent_id' => '234',
					),
					array('translation' => array('en' => array(
								'name' => 'STOP SWITCHES (BRAKE SWITCH)',),), 'sort_order' => '0',
						'parent_id' => '234',
					),
					array('translation' => array('en' => array(
								'name' => 'CLUTCH SWITCHES',),), 'sort_order' => '0',
						'parent_id' => '234',
					),
					array('translation' => array('en' => array(
								'name' => 'THERMAL SWITCHES',),), 'sort_order' => '0',
						'parent_id' => '234',
					),
					array('translation' => array('en' => array(
								'name' => 'KILL SWITCH',),), 'sort_order' => '0',
						'parent_id' => '234',
					),

					array('translation' => array('en' => array(
								'name' => 'ENGINE VALVE',),), 'sort_order' => '0',
						'parent_id' => '240',
					), 
					array('translation' => array('en' => array(
								'name' => 'VALVE SPRINGS',),), 'sort_order' => '0',
						'parent_id' => '240',
					), 
					array('translation' => array('en' => array(
								'name' => 'RETAINERS',),), 'sort_order' => '0',
						'parent_id' => '240',
					), 
					array('translation' => array('en' => array(
								'name' => 'VALVE GUIDES',),), 'sort_order' => '0',
						'parent_id' => '240',
					), 
					array('translation' => array('en' => array(
								'name' => 'KEEPERS',),), 'sort_order' => '0',
						'parent_id' => '240',
					), 
					array('translation' => array('en' => array(
								'name' => 'VALVE SHIMS',),), 'sort_order' => '0',
						'parent_id' => '240',
					), 
					array('translation' => array('en' => array(
								'name' => 'VALVE STEM SEALS',),), 'sort_order' => '0',
						'parent_id' => '240',
					), 

					array('translation' => array('en' => array(
								'name' => 'PISTON KITS',),), 'sort_order' => '0',
						'parent_id' => '241',
					), 
					array('translation' => array('en' => array(
								'name' => 'RINGS',),), 'sort_order' => '0',
						'parent_id' => '241',
					),
					array('translation' => array('en' => array(
								'name' => 'PINS',),), 'sort_order' => '0',
						'parent_id' => '241',
					),
					array('translation' => array('en' => array(
								'name' => 'CIRCLIPS',),), 'sort_order' => '0',
						'parent_id' => '241',
					),

					array('translation' => array('en' => array(
								'name' => 'CLUTCH FRICTION PLATES',),), 'sort_order' => '0',
						'parent_id' => '242',
					), 
					array('translation' => array('en' => array(
								'name' => 'CLUTCH STEEL PLATES',),), 'sort_order' => '0',
						'parent_id' => '242',
					),
					array('translation' => array('en' => array(
								'name' => 'CLUTCH SPRING SETS',),), 'sort_order' => '0',
						'parent_id' => '242',
					), 
					array('translation' => array('en' => array(
								'name' => 'CLUTCH KITS',),), 'sort_order' => '0',
						'parent_id' => '242',
					),
					array('translation' => array('en' => array(
								'name' => 'CLUTCH PULLERS',),), 'sort_order' => '0',
						'parent_id' => '242',
					),

					array('translation' => array('en' => array(
								'name' => 'O-RINGS',),), 'sort_order' => '0',
						'parent_id' => '245',
					), 
					array('translation' => array('en' => array(
								'name' => 'OIL DIPSTICKS',),), 'sort_order' => '0',
						'parent_id' => '245',
					), 
					array('translation' => array('en' => array(
								'name' => 'OIL DRAIN BOLT SUMP PLUG',),), 'sort_order' => '0',
						'parent_id' => '245',
					), 
					array('translation' => array('en' => array(
								'name' => 'OIL SEALS',),), 'sort_order' => '0',
						'parent_id' => '245',
					), 
					array('translation' => array('en' => array(
								'name' => 'SMALL END BEARINGS',),), 'sort_order' => '0',
						'parent_id' => '245',
					), 
					array('translation' => array('en' => array(
								'name' => 'TAPPET COVERS',),), 'sort_order' => '0',
						'parent_id' => '245',
					), 
					array('translation' => array('en' => array(
								'name' => 'WOODRUFF KEYS',),), 'sort_order' => '0',
						'parent_id' => '245',
					), 
					array('translation' => array('en' => array(
								'name' => 'GEAR SHAFT',),), 'sort_order' => '0',
						'parent_id' => '245',
					), 

					array('translation' => array('en' => array(
								'name' => 'HEADER CLAMPS',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 
					array('translation' => array('en' => array(
								'name' => 'EXHAUST SPRINGS',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 
					array('translation' => array('en' => array(
								'name' => 'MUFFLER CLAMPS',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 
					array('translation' => array('en' => array(
								'name' => 'ADAPTERS',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 
					array('translation' => array('en' => array(
								'name' => 'BRACKETS',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 
					array('translation' => array('en' => array(
								'name' => 'HEAT SHIELDS (PIPE GUARDS)',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 
					array('translation' => array('en' => array(
								'name' => 'MUFFLER PACKING',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 
					array('translation' => array('en' => array(
								'name' => 'EXHAUST PIPE WRAP',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 
					array('translation' => array('en' => array(
								'name' => 'HEAT SHIELD CLOTH',),), 'sort_order' => '0',
						'parent_id' => '250',
					), 

					array('translation' => array('en' => array(
								'name' => 'HONDA',),), 'sort_order' => '0',
						'parent_id' => '251',
					), 
					array('translation' => array('en' => array(
								'name' => 'YAMAHA',),), 'sort_order' => '0',
						'parent_id' => '251',
					), 
					array('translation' => array('en' => array(
								'name' => 'SUZUKI',),), 'sort_order' => '0',
						'parent_id' => '251',
					), 
					array('translation' => array('en' => array(
								'name' => 'KAWASAKI',),), 'sort_order' => '0',
						'parent_id' => '251',
					), 
					array('translation' => array('en' => array(
								'name' => 'OTHERS',),), 'sort_order' => '0',
						'parent_id' => '251',
					), 

					array('translation' => array('en' => array(
								'name' => 'Velocity Stacks',),), 'sort_order' => '0',
						'parent_id' => '266',
					), 
					array('translation' => array('en' => array(
								'name' => 'HOSE CLAMPS',),), 'sort_order' => '0',
						'parent_id' => '266',
					), 
					array('translation' => array('en' => array(
								'name' => 'OIL TANK CAP',),), 'sort_order' => '0',
						'parent_id' => '266',
					), 
					array('translation' => array('en' => array(
								'name' => 'OIL LEVER GAUGE',),), 'sort_order' => '0',
						'parent_id' => '266',
					), 
					array('translation' => array('en' => array(
								'name' => 'CARB MANIFOLD FLANGE  ',),), 'sort_order' => '0',
						'parent_id' => '266',
					), 
					array('translation' => array('en' => array(
								'name' => 'DRAIN PLUG',),), 'sort_order' => '0',
						'parent_id' => '266',
					), 

					array('translation' => array('en' => array(
								'name' => 'HONDA',),), 'sort_order' => '0',
						'parent_id' => '267',
					), 
					array('translation' => array('en' => array(
								'name' => 'YAMAHA',),), 'sort_order' => '0',
						'parent_id' => '267',
					), 
					array('translation' => array('en' => array(
								'name' => 'SUZUKI',),), 'sort_order' => '0',
						'parent_id' => '267',
					), 
					array('translation' => array('en' => array(
								'name' => 'KAWASAKI',),), 'sort_order' => '0',
						'parent_id' => '267',
					), 
					array('translation' => array('en' => array(
								'name' => 'OTHERS',),), 'sort_order' => '0',
						'parent_id' => '267',
					), 

					array('translation' => array('en' => array(
								'name' => 'HONDA',),), 'sort_order' => '0',
						'parent_id' => '268',
					), 
					array('translation' => array('en' => array(
								'name' => 'YAMAHA',),), 'sort_order' => '0',
						'parent_id' => '268',
					), 
					array('translation' => array('en' => array(
								'name' => 'SUZUKI',),), 'sort_order' => '0',
						'parent_id' => '268',
					), 
					array('translation' => array('en' => array(
								'name' => 'KAWASAKI',),), 'sort_order' => '0',
						'parent_id' => '268',
					), 
					array('translation' => array('en' => array(
								'name' => 'OTHERS',),), 'sort_order' => '0',
						'parent_id' => '268',
					), 

					array('translation' => array('en' => array(
								'name' => '1" BARS',),), 'sort_order' => '0',
						'parent_id' => '275',
					), 
					array('translation' => array('en' => array(
								'name' => '7/8" BARS',),), 'sort_order' => '0',
						'parent_id' => '275',
					), 
					array('translation' => array('en' => array(
								'name' => 'ALUMINUM 7/8" BARS',),), 'sort_order' => '0',
						'parent_id' => '275',
					), 

					array('translation' => array('en' => array(
								'name' => 'GRIPS',),), 'sort_order' => '0',
						'parent_id' => '278',
					), 
					array('translation' => array('en' => array(
								'name' => 'GRIP DONUTS',),), 'sort_order' => '0',
						'parent_id' => '278',
					), 
					array('translation' => array('en' => array(
								'name' => 'HEATED GRIPS',),), 'sort_order' => '0',
						'parent_id' => '278',
					), 

					array('translation' => array('en' => array(
								'name' => 'FRONT',),), 'sort_order' => '0',
						'parent_id' => '279',
					), 
					array('translation' => array('en' => array(
								'name' => 'REAR',),), 'sort_order' => '0',
						'parent_id' => '279',
					), 

					array('translation' => array('en' => array(
								'name' => 'THROTTLE ASSEMBLIES',),), 'sort_order' => '0',
						'parent_id' => '279',
					), 
					array('translation' => array('en' => array(
								'name' => 'THROTTLE KITS',),), 'sort_order' => '0',
						'parent_id' => '279',
					), 
					array('translation' => array('en' => array(
								'name' => 'THROTTLE TUBES',),), 'sort_order' => '0',
						'parent_id' => '279',
					), 

					array('translation' => array('en' => array(
								'name' => 'SHORTY',),), 'sort_order' => '0',
						'parent_id' => '300',
					), 
					array('translation' => array('en' => array(
								'name' => 'RUBBER INLAY',),), 'sort_order' => '0',
						'parent_id' => '300',
					), 

					array('translation' => array('en' => array(
								'name' => 'CABLE ADJUSTER',),), 'sort_order' => '0',
						'parent_id' => '301',
					), 
					array('translation' => array('en' => array(
								'name' => 'FOAM LEVER SLEEVES',),), 'sort_order' => '0',
						'parent_id' => '301',
					), 

					array('translation' => array('en' => array(
								'name' => 'GEAR LEVERS',),), 'sort_order' => '0',
						'parent_id' => '303',
					), 
					array('translation' => array('en' => array(
								'name' => 'GEAR LEVER RUBBERS',),), 'sort_order' => '0',
						'parent_id' => '303',
					), 

					array('translation' => array('en' => array(
								'name' => 'COMPLETE ASSEMBLY',),), 'sort_order' => '0',
						'parent_id' => '322',
					), 
					array('translation' => array('en' => array(
								'name' => 'SHELLS & KITS',),), 'sort_order' => '0',
						'parent_id' => '322',
					), 
					array('translation' => array('en' => array(
								'name' => 'LIGHT UNITS',),), 'sort_order' => '0',
						'parent_id' => '322',
					), 
					array('translation' => array('en' => array(
								'name' => 'HEADLIGHT LIGHT BRACKETS',),), 'sort_order' => '0',
						'parent_id' => '322',
					), 

					array('translation' => array('en' => array(
								'name' => 'HONDA',),), 'sort_order' => '0',
						'parent_id' => '323',
					), 
					array('translation' => array('en' => array(
								'name' => 'YAMAHA',),), 'sort_order' => '0',
						'parent_id' => '323',
					), 
					array('translation' => array('en' => array(
								'name' => 'SUZUKI',),), 'sort_order' => '0',
						'parent_id' => '323',
					), 
					array('translation' => array('en' => array(
								'name' => 'KAWASAKI',),), 'sort_order' => '0',
						'parent_id' => '323',
					), 
					array('translation' => array('en' => array(
								'name' => 'EUROPEAN',),), 'sort_order' => '0',
						'parent_id' => '323',
					), 
					array('translation' => array('en' => array(
								'name' => 'UNIVERSAL',),), 'sort_order' => '0',
						'parent_id' => '323',
					), 
					array('translation' => array('en' => array(
								'name' => 'CUSTOM',),), 'sort_order' => '0',
						'parent_id' => '323',
					), 

					array('translation' => array('en' => array(
								'name' => 'INDICATORS',),), 'sort_order' => '0',
						'parent_id' => '324',
					), 
					array('translation' => array('en' => array(
								'name' => 'INDICATOR CLEAR LENS KIT',),), 'sort_order' => '0',
						'parent_id' => '324',
					), 
					array('translation' => array('en' => array(
								'name' => 'INDICATOR LENSES',),), 'sort_order' => '0',
						'parent_id' => '324',
					), 
					array('translation' => array('en' => array(
								'name' => 'INDICATOR MOUNTS',),), 'sort_order' => '0',
						'parent_id' => '324',
					), 
					array('translation' => array('en' => array(
								'name' => 'INDY SPACERS',),), 'sort_order' => '0',
						'parent_id' => '324',
					), 

					array('translation' => array('en' => array(
								'name' => 'SWING ARM BUSHINGS',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 
					array('translation' => array('en' => array(
								'name' => 'STEERING BEARINGS',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 
					array('translation' => array('en' => array(
								'name' => 'STEERING DAMPERS',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 
					array('translation' => array('en' => array(
								'name' => 'FORK BLEEDER NIPPLES',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 
					array('translation' => array('en' => array(
								'name' => 'FORK BUSH KITS',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 
					array('translation' => array('en' => array(
								'name' => 'FORK OIL & DUST SEALS',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 
					array('translation' => array('en' => array(
								'name' => 'FORK REPAIR KITS',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 
					array('translation' => array('en' => array(
								'name' => 'FORKS & STANCHIONS',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 
					array('translation' => array('en' => array(
								'name' => 'FORK PROTECTORS',),), 'sort_order' => '0',
						'parent_id' => '333',
					), 

					array('translation' => array('en' => array(
								'name' => 'WHEEL BEARINGS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'WHEEL SEALS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'AXELS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'RIM PROTECTORS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'PRESSRE GAUGES',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'REPAIR KITS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'TYRE VALVES & CAPS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'TYRE WARMERS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'WHEEL WEIGHTS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'CHAIN GUIDES',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 
					array('translation' => array('en' => array(
								'name' => 'ROLLERS',),), 'sort_order' => '0',
						'parent_id' => '349',
					), 

					array('translation' => array('en' => array(
								'name' => 'CASTLE NUTS',),), 'sort_order' => '0',
						'parent_id' => '384',
					), 
					array('translation' => array('en' => array(
								'name' => 'CYLINDER NUTS',),), 'sort_order' => '0',
						'parent_id' => '384',
					), 
					array('translation' => array('en' => array(
								'name' => 'FLANGE NUTS',),), 'sort_order' => '0',
						'parent_id' => '384',
					), 
					array('translation' => array('en' => array(
								'name' => 'NYLOC NUTS',),), 'sort_order' => '0',
						'parent_id' => '384',
					), 
					array('translation' => array('en' => array(
								'name' => 'PLAIN NUTS',),), 'sort_order' => '0',
						'parent_id' => '384',
					), 

					array('translation' => array('en' => array(
								'name' => 'STAINLESS HEXAGON BOLTS',),), 'sort_order' => '0',
						'parent_id' => '385',
					), 
					array('translation' => array('en' => array(
								'name' => 'ZINC HEXAGON BOLTS',),), 'sort_order' => '0',
						'parent_id' => '385',
					), 
					array('translation' => array('en' => array(
								'name' => 'Allen Bolts Countersunk Section ',),), 'sort_order' => '0',
						'parent_id' => '385',
					), 
					array('translation' => array('en' => array(
								'name' => 'Bolts Hexagon Section ',),), 'sort_order' => '0',
						'parent_id' => '385',
					), 
					array('translation' => array('en' => array(
								'name' => 'CHROME HEXAGON BOLTS',),), 'sort_order' => '0',
						'parent_id' => '385',
					), 

					array('translation' => array('en' => array(
								'name' => 'ALLEN SCREWS',),), 'sort_order' => '0',
						'parent_id' => '386',
					), 
					array('translation' => array('en' => array(
								'name' => 'STAINLESS ALLEN SCREWS',),), 'sort_order' => '0',
						'parent_id' => '386',
					), 
					array('translation' => array('en' => array(
								'name' => 'STAINLESS BUTTON ALLEN SCREWS',),), 'sort_order' => '0',
						'parent_id' => '386',
					), 
					
					array('translation' => array('en' => array(
								'name' => 'STAINLESS PAN HEAD SCREWS',),), 'sort_order' => '0',
						'parent_id' => '386',
					), 
					array('translation' => array('en' => array(
								'name' => 'PAN HEAD SCREWS',),), 'sort_order' => '0',
						'parent_id' => '386',
					), 
					array('translation' => array('en' => array(
								'name' => 'LARGE PAN HEAD SCREWS',),), 'sort_order' => '0',
						'parent_id' => '386',
					), 
					array('translation' => array('en' => array(
								'name' => 'PHILLIP PAN HEAD',),), 'sort_order' => '0',
						'parent_id' => '386',
					), 
					/*HANDLEBAR & CONTROLS*/
					/*537*/
					array('translation' => array('en' => array(
								'name' => 'STREET',),), 'sort_order' => '0',
						'parent_id' => '0',
					),  
							/*538*/
					array('translation' => array('en' => array(
								'name' => 'DIRT',),), 'sort_order' => '0',
						'parent_id' => '0',
					),  
							/*539*/
					array('translation' => array('en' => array(
								'name' => 'ATV',),), 'sort_order' => '0',
						'parent_id' => '0',
					), 
							/*539*/
					array('translation' => array('en' => array(
								'name' => 'SNOWMOBILE',),), 'sort_order' => '0',
						'parent_id' => '0',
					), 
		);

		$count = 0; 
		foreach ($categories as $category)
		{ 
			$cat = New Category;  
			$cat->parent_id 	 = $category['parent_id'];
			$cat->sort_order     = $category['sort_order'];
			$cat->save();

			foreach ($category['translation'] as $key => $val)
			{
				$locale = Locale::where('language', '=', $key)->first();

				$catTranslation = new CategoryTranslation;
				$catTranslation->category_id = $cat->id;
				$catTranslation->locale_id   = $locale->id;
				$catTranslation->name        = $val['name'];
				$catTranslation->save();
			}
		}
	}

}