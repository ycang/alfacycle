<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder 
{
	public function run()
	{
		DB::table('users')->delete();

		$users = array(
			array(
				'name'     => 'alfacycle',
				'email'    => 'admin@alfacycle.com',
				'role'     => 'admin',
				'status'   => '2',
				'password' => Hash::make('alfacycle.1234!'),
			),
			array(
				'name'     => 'ang yeow chuan',
				'email'    => 'yeowchuan89@gmail.com',
				'role'     => 'member',
				'status'   => '2',
				'password' => Hash::make('123456'),
			),
		);

		foreach ($users as $user)
		{
			User::create($user);
		}
	}

}