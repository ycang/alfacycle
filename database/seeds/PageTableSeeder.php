<?php

use Illuminate\Database\Seeder;
use App\Models\Locale;
use App\Models\Page;
use App\Models\PageTranslation;

class PageTableSeeder extends Seeder 
{
	public function run()
	{
   		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('pages')->delete();

		$pages = array(
			// Home
			array(
				'name'        => 'Home',
				'slug'        => '/',
				'translation' => array( 
					'en' => array(
						'desc'    => 'Home page.',
						'content' => '',
					),
				),
			), 
		);

		foreach ($pages as $page)
		{
			$pg = New Page;
			$pg->name = $page['name'];
			$pg->slug = $page['slug'];
			$pg->save();

			foreach ($page['translation'] as $key => $val)
			{
				$locale = Locale::where('language', '=', $key)->first();

				$pgTranslation = new PageTranslation;
				$pgTranslation->page_id   = $pg->id;
				$pgTranslation->locale_id = $locale->id;
				$pgTranslation->desc      = $val['desc'];
				$pgTranslation->content   = $val['content'];
				$pgTranslation->save();
			}
		}
    	DB::statement('SET FOREIGN_KEY_CHECKS=1');
	}

}